$web = Get-SPWeb -Identity "siteCollectionURL"
$field = $web.Fields["ELMDisplayOrder"]
$web = Get-SPWeb -Identity "URL where propagation of content type is required"
$list = $web.Lists["ListName in which content type is used and you want to update"]
$contentTypes = $list.ContentTypes
$ct = $contentTypes[position of your content type]
$link = new-object Microsoft.Sharepoint.SPFieldLink $field
$ct.FieldLinks.Add($link);
$ct.Update()