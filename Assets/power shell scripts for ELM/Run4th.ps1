Add-PsSnapin Microsoft.SharePoint.PowerShell

## SharePoint DLL 
[void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SharePoint") 

#Creating Sub Sites in top site collection.
Write-Output " "
Write-Output "Creating Sub Sites"

$SiteCollectionURL = "http://ggnp-spdev04:3000/ar/careers"

$SiteCollectionTemplate = "BLANKINTERNETCONTAINER#0" 

$SiteCollectionLanguage = 1033

$SubSites = @("Why Join ELM")
$SubSitesUrl = @("join")

for($i=0 ; $i -lt $SubSites.count ; $i++)
{
$SiteUrl = ""
$SiteUrl = $SiteCollectionURL + "/" 
$SiteUrl = $SiteUrl += $SubSitesUrl[$i]
Write-Output " "
#Write-Output "Creating Site for " += $SubSites[$i]
Write-Output " "
New-SPWeb $SiteUrl -Template $SiteCollectionTemplate -Name $SubSites[$i]   -AddToTopNav -Language $SiteCollectionLanguage
Write-Output " "
#Write-Output "Site Created for " += $SubSites[$i]
Write-Output " "
}

Remove-PsSnapin Microsoft.SharePoint.PowerShell
