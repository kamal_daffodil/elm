﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageRotatorUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.ImageRotator.ImageRotatorUserControl" %>
<link href="/_layouts/ELM/ImageRotator/flexslider.css" rel="stylesheet" />
<script src="/_layouts/ELM/ImageRotator/jquery.flexslider.js" type="text/javascript"></script>
<div class="flexslider loading" runat="server" id="bannerRotator">
</div>
<script type="text/javascript">
    var flexcache = $.fn.flexslider;
</script>
<script type="text/javascript">
    (function (cash) {
        $(window).load(function () {
            $.fn.flexslider = flexcache;
            $('.flexslider').flexslider({
                animation: "slide",
                start: function (slider) {
                    slider.removeClass('loading');
                }
            });
        });
    })(jQuery);

</script>
