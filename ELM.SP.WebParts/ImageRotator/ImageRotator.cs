﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace ELM.SP.WebParts.ImageRotator
{
    [ToolboxItemAttribute(false)]
    public class ImageRotator : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/ELM.SP.WebParts/ImageRotator/ImageRotatorUserControl.ascx";

        protected override void CreateChildControls()
        {
            ImageRotatorUserControl imageRotatorUC = (ImageRotatorUserControl)Page.LoadControl(_ascxPath);
            if (imageRotatorUC != null)
            {
                imageRotatorUC.oImageRotator = this;
                Controls.Add(imageRotatorUC);
            }
        }
        public static string SourceUri;
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Picture Library Url"),
        WebDescription("Please Enter a Sample Text")]
        public string _SourceUri
        {
            get { return SourceUri; }
            set { SourceUri = value; }
        }
    }
}
