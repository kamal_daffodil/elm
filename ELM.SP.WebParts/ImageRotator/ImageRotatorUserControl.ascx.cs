﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Text;
using Microsoft.SharePoint.Publishing.Fields;
using ELM.SP.Library;
namespace ELM.SP.WebParts.ImageRotator
{
    public partial class ImageRotatorUserControl : UserControl
    {
        #region Properties
        public ImageRotator oImageRotator { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (SPSite site = new Microsoft.SharePoint.SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        string gallerySource = oImageRotator._SourceUri;

                        if (string.IsNullOrEmpty(gallerySource))
                        {
                            gallerySource = CoreConstants.Lists.Banner.URL;
                        }
                        try
                        {
                            string serverUrl = string.Format("{0}/Lists/", web.Url);
                            SPList bannerList = web.GetList(SPUrlUtility.CombineUrl(serverUrl, gallerySource));
                            GetData(bannerList);
                        }
                        catch(Exception ex)
                        {
                            LoggerService.LogError(LoggerService.LOG_ERROR, string.Format("Exception - {0}", ex.Message));
                            bannerRotator.InnerHtml = CoreConstants.Lists.Banner.NoItemString;
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        #endregion

        #region Private members
        /// <summary>
        /// Method to get data for image slider from Banner Items List
        /// </summary>
        /// <param name="bannerList"></param>
        private void GetData(SPList bannerList) 
        {
            if (bannerList != null)
            {
                string toDate = (SPUtility.CreateISO8601DateTimeFromSystemDateTime(System.DateTime.Now.Date));
                string redirectUrl = CoreConstants.Lists.Banner.Fields.BannerRedirectURL;
                SPQuery query = new SPQuery();
                query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy><Where><And><Leq><FieldRef Name='{1}' /><Value IncludeTimeValue='FALSE' Type='DateTime'>{2}</Value></Leq><Geq><FieldRef Name='{3}' /><Value IncludeTimeValue='FALSE' Type='DateTime'>{2}</Value></Geq></And></Where>", CoreConstants.Lists.Banner.Fields.BannerDisplayOrder, CoreConstants.Lists.Banner.Fields.BannerStartDate, toDate, CoreConstants.Lists.Banner.Fields.BannerEndDate);
                SPListItemCollection bannerItems = bannerList.GetItems(query);
                if (bannerItems != null && bannerItems.Count > 0)
                {
                    StringBuilder bannerRotatorHtml = new StringBuilder();
                    bannerRotatorHtml.Append("<ul class=\"slides\">");
                    foreach (SPListItem bannerItem in bannerItems)
                    {
                        string rootUrl = SPContext.Current.Site.Url;
                        ImageFieldValue loc = bannerItem[CoreConstants.Lists.Banner.Fields.BannerImage] as ImageFieldValue;
                        string imageLocation = Convert.ToString(loc.ImageUrl);
                        if (imageLocation.Contains("http://"))
                        {
                            imageLocation = string.Format("{1}1&imageUrl={0}", Convert.ToString(loc.ImageUrl),CoreConstants.ImageHandlerURL);
                        }
                        else
                        {
                            imageLocation = string.Format("{2}1&imageUrl={0}{1}", rootUrl, Convert.ToString(loc.ImageUrl), CoreConstants.ImageHandlerURL);
                        }

                        string redirectLoc = Convert.ToString(bannerItem[redirectUrl]);
                        if (string.IsNullOrEmpty(redirectLoc))
                        {
                            redirectLoc = CoreConstants.Lists.Banner.VoidURL;
                        }
                        else
                        {
                            redirectLoc = redirectLoc.Split(',')[0];
                        }
                        bannerRotatorHtml.Append(string.Format("<li><a href=\"{0}\" target='_blank' ><img src=\"{1}\" /></a></li>", redirectLoc, imageLocation));
                    }
                    bannerRotatorHtml.Append("</ul>");
                    bannerRotator.InnerHtml = bannerRotatorHtml.ToString();
                }
            }
            else
            {
                bannerRotator.InnerHtml = CoreConstants.Lists.Banner.NoItemString;
            }
        }
        #endregion
    }
}
