﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace ELM.SP.WebParts.VisualWebPart1
{
    public partial class VisualWebPart1UserControl : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb currentWeb = currentSite.OpenWeb())
                {
                    string serverUrl = currentWeb.Url;
                    SPList socialMedia = currentWeb.GetList(SPUrlUtility.CombineUrl(serverUrl, "Lists/SocialMedia"));

                    SPQuery query = new SPQuery();
                    query.Query = "<OrderBy><FieldRef Name='ELMDisplayOrder' Ascending='TRUE'/></OrderBy>";
                    SPListItemCollection socialMediaItems = socialMedia.GetItems(query);

                    repSocialStaticLinks.DataSource = socialMediaItems.GetDataTable();
                    repSocialStaticLinks.DataBind();

                }
            }
            

        }
    }
}
