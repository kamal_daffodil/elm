﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.IO;
using System.Net;
using System.Web;
using System.Drawing.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using ELM.SP.Library;

namespace ELM.SP.WebParts
{

    /// <summary>
    /// Image handler for the Thumbnail view
    /// </summary>
    public partial class thumbnail : UnsecuredLayoutsPageBase
    {
        protected override bool AllowAnonymousAccess { get { return true; } }
        /// <summary>
        /// Page_Load Events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["imageUrl"] == null) return;
            if (string.IsNullOrEmpty(Request.QueryString["imageUrl"])) return;
            SPSecurity.RunWithElevatedPrivileges(delegate
                                                     {
                                                         var pictureUrl = Convert.ToString(Request.QueryString["imageUrl"]);

                                                         int queryStringIndex = pictureUrl.IndexOf('?');
                                                         if (queryStringIndex > 0)
                                                         {
                                                             pictureUrl = pictureUrl.Substring(0, queryStringIndex);
                                                         }

                                                         if (pictureUrl.Contains(","))
                                                         {
                                                             pictureUrl = Convert.ToString(pictureUrl).Split(',')[0];
                                                         }

                                                         if (!String.IsNullOrEmpty(pictureUrl))
                                                         {

                                                             var pictureExt = Path.GetExtension(pictureUrl);
                                                             if (pictureExt == null) return;
                                                             var extension = pictureExt.Remove(0, 1).Trim();
                                                             if (!extension.ToLower().Equals("png") &&
                                                                 !extension.ToLower().Equals("jpg") &&
                                                                 !extension.ToLower().Equals("gif")) return;
                                                             var imgByte = GetImageByteArr(Convert.ToString(pictureUrl));

                                                             using (var memoryStream = new MemoryStream())
                                                             {
                                                                 memoryStream.Write(imgByte, 0, imgByte.Length);
                                                                 using (var imgOriginal = System.Drawing.Image.FromStream(memoryStream))
                                                                 {
                                                                     var imgHeight = 199;
                                                                     var imgWidth = 300;

                                                                     var actualAspectRatio = Convert.ToDecimal(imgWidth) / imgHeight;

                                                                     if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
                                                                     {
                                                                         var mode = Request.QueryString["mode"];


                                                                         switch (mode)
                                                                         {
                                                                             case "1": imgWidth = 1000; imgHeight = 326; break;
                                                                             case "2": imgWidth = 280; imgHeight = 150; break;
                                                                             case "3": imgWidth = 300; imgHeight = 199; break;
                                                                             default: break;
                                                                         }
                                                                     }
                                                                     var imageWidth = imgOriginal.Width > imgWidth ? imgWidth : imgOriginal.Width;
                                                                     var imageHeight = imgHeight;

                                                                     using (var bitmap = new Bitmap(imageWidth, imageHeight, imgOriginal.PixelFormat))
                                                                     {
                                                                         bitmap.SetResolution(imgOriginal.HorizontalResolution, imgOriginal.VerticalResolution);
                                                                         using (var mGraphics = Graphics.FromImage(bitmap))
                                                                         {
                                                                             mGraphics.SmoothingMode = SmoothingMode.HighQuality;
                                                                             mGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                                                             mGraphics.DrawImage(imgOriginal, 0, 0, bitmap.Width, bitmap.Height);

                                                                             if (extension.Equals("jpg"))
                                                                             {
                                                                                 extension = "jpeg";
                                                                             }
                                                                             Response.ContentType = string.Format("image/{0}", extension);
                                                                             bitmap.Save(Response.OutputStream, GetImageFormat(extension.ToLower()));
                                                                         }
                                                                     }
                                                                 }
                                                             }
                                                         }
                                                     });
        }


        /// <summary>
        /// Get image format from extension of image
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        private static ImageFormat GetImageFormat(string extension)
        {
            ImageFormat format;
            switch (extension)
            {
                case "jpeg": format = ImageFormat.Jpeg;
                    break;

                case "png": format = ImageFormat.Png;
                    break;
                case "gif": format = ImageFormat.Gif;
                    break;
                default: format = ImageFormat.Jpeg;
                    break;
            }
            return format;
        }

        /// <summary>
        /// Get the image as byte array
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private static byte[] GetImageByteArr(String img)
        {
            byte[] imgByte = null;
            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    imgByte = webClient.DownloadData(img);
                }
            }
            catch (Exception ex)
            {
               LoggerService.LogError(LoggerService.LOG_ERROR, String.Format("Exception Caught:-{0}", ex));
            }
            return imgByte;
        }
    }
}
