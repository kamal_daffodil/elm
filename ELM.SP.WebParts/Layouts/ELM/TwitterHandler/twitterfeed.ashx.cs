﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Microsoft.SharePoint.Utilities;
using ELM.SP.Library;
namespace ELM.SP.WebParts
{
    public class twitterfeed : IHttpHandler
    {
        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var oauth_token = string.Empty;
            var oauth_token_secret = string.Empty;
            var oauth_consumer_key = string.Empty;
            var oauth_consumer_secret = string.Empty;
            var screen_name = string.Empty;

            using (SPSite spsite = new Microsoft.SharePoint.SPSite(SPContext.Current.Web.Url))
            {
                using (SPWeb spWeb = spsite.OpenWeb())
                {
                    string serverUrl = spWeb.Url;
                    SPList twitterAccount = spWeb.GetList(SPUrlUtility.CombineUrl(serverUrl, CoreConstants.Lists.TwitterFeeds.URL));
                    if (twitterAccount != null)
                    {
                        SPQuery getAccountDetails = new SPQuery();
                        getAccountDetails.Query = string.Format("<Where><Eq><FieldRef Name={0} /><Value Type='Text'>{1}</Value></Eq></Where>", CoreConstants.Lists.TwitterFeeds.Fields.TwitterKey, CoreConstants.Lists.TwitterFeeds.TwitterKeyValue);
                        getAccountDetails.RowLimit = 1;
                        SPListItemCollection listResult = twitterAccount.GetItems(getAccountDetails);
                        if (listResult != null)
                        {
                            foreach (SPListItem accTweets in listResult)
                            {
                                oauth_token = accTweets[CoreConstants.Lists.TwitterFeeds.Fields.OAuthToken].ToString();
                                oauth_token_secret = accTweets[CoreConstants.Lists.TwitterFeeds.Fields.OAuthTokenSecret].ToString();
                                oauth_consumer_key = accTweets[CoreConstants.Lists.TwitterFeeds.Fields.OAuthConsumerKey].ToString();
                                oauth_consumer_secret = accTweets[CoreConstants.Lists.TwitterFeeds.Fields.OAuthConsumerSecret].ToString();
                                screen_name = accTweets[CoreConstants.Lists.TwitterFeeds.Fields.ScreenName].ToString();
                            }
                        }
                    }
                }
            }
            try
            {
                  var oauth_version = CoreConstants.Lists.TwitterFeeds.OAuthVersion;
                var oauth_signature_method = CoreConstants.Lists.TwitterFeeds.SignatureMethod;

                // unique request details
                var oauth_nonce = Convert.ToBase64String(
                    new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
                var timeSpan = DateTime.UtcNow
                    - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

                var resource_url = CoreConstants.Lists.TwitterFeeds.ResourceURL;

                var baseString = string.Format("oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}",
                                            oauth_consumer_key,
                                            oauth_nonce,
                                            oauth_signature_method,
                                            oauth_timestamp,
                                            oauth_token,
                                            oauth_version,
                                             Uri.EscapeDataString(screen_name)
                                            );

                baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

                var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
                                        "&", Uri.EscapeDataString(oauth_token_secret));

                string oauth_signature;
                using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
                {
                    oauth_signature = Convert.ToBase64String(
                        hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
                }

                // create the request header
                var authHeader = string.Format("OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", oauth_token=\"{4}\", oauth_signature=\"{5}\", oauth_version=\"{6}\"",
                                        Uri.EscapeDataString(oauth_nonce),
                                        Uri.EscapeDataString(oauth_signature_method),
                                        Uri.EscapeDataString(oauth_timestamp),
                                        Uri.EscapeDataString(oauth_consumer_key),
                                        Uri.EscapeDataString(oauth_token),
                                        Uri.EscapeDataString(oauth_signature),
                                        Uri.EscapeDataString(oauth_version)
                                );

                // make the request

                ServicePointManager.Expect100Continue = false;
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
              
                var postBody = string.Format("screen_name={0}", Uri.EscapeDataString(screen_name));
                resource_url += "?" + postBody;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
                request.Headers.Add("Authorization", authHeader);
                request.Method = "GET";
                request.ContentType = "application/x-www-form-urlencoded";

                WebResponse response = request.GetResponse();
                string responseData = new StreamReader(response.GetResponseStream()).ReadToEnd();
                HttpContext.Current.Response.Write(responseData);
            }
            catch (Exception ex) {
                var errorMessage = String.Format("Exception - {0}",ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
    }
}
