﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using ELM.SP.Library;
namespace ELM.SP.WebParts.ContactForm
{
    public partial class ContactFormUserControl : UserControl
    {
         

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                pnlSuccessDiv.Visible = false;
                pnlContact.Visible = false;
            }
            else
            {
                if (hdnViewID.Value == "0")
                {
                    pnlSuccessDiv.Visible = false;
                    pnlContact.Visible = true;
                }
                else
                {
                    pnlSuccessDiv.Visible = true;
                }
                uint locale = (uint)SPContext.Current.Web.Locale.LCID;
                if (!IsPostBack)
                {
                    hdnCaptchaKey.Value = Utility.Encrypt(Utility.GetRandomCaptchaValue());
                    imgCaptcha.ImageUrl = string.Format("{0}{1}", CoreConstants.CaptchaHandlerURL, hdnCaptchaKey.Value);
                    //Label Inner text From resource file
                    SetLabelText(locale);
                    //Validator error messages from resource file
                    SetValidatorMessages(locale);
                    //DropDown list items from resource files
                    FilldropDown(locale);
                    selctRequestType.SelectedIndex = 2;
                }
            }
        }
        /// <summary>
        /// Send Button Click event to contact for support
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSend_Click(Object sender, EventArgs e)
        {
            lblWrongCode.Visible = false;
             
            uint localeCurrent = (uint)SPContext.Current.Web.Locale.LCID;
            string hdnCaptchaKeyText = Utility.Decrypt(hdnCaptchaKey.Value);
            if (hdnCaptchaKeyText == txtCaptchaCode.Text)
            {
                try
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                   {
                       using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                       {
                           using (SPWeb currentWeb = currentSite.OpenWeb())
                           {
                               string rootUrl = currentSite.RootWeb.Url;
                               string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                               SPList contactRecordList = currentWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}", rootUrl, sourceVariation), CoreConstants.Lists.ContactFormRecord.URL));
                               SPListItemCollection listItems = contactRecordList.Items;
                               SPListItem item = listItems.Add();

                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactCompanyName] = textCompanyName.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactEmail] = textEmail.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactExtension] = textExtension.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactFax] = textFax.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactJobTitle] = textJobTitle.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactMessage] = textMessage.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactMobile] = textMobile.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactTitle] = textName.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactPhone] = textPhone.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactRequestType] = selctRequestType.SelectedItem.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactService] = selctService.SelectedItem.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactPersonTitle] = selctTitle.SelectedItem.Text;
                               item[CoreConstants.Lists.ContactFormRecord.Fields.ContactBranchCity] = selctMainBranch.SelectedItem.Text;
                               currentWeb.AllowUnsafeUpdates = true;
                               item.Update();
                               currentWeb.AllowUnsafeUpdates = false;
                           }
                       }
                   });
                }
                catch (Exception exception)
                {
                    var errorMessage = string.Format("Exception Caught - {0}", exception.Message);
                    LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
                }
                pnlSuccessDiv.Visible = true;
                ClearFields();
            }
            else
            {
                lblWrongCode.Visible = true;
                validatorCaptcha.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Wrong_CaptchaCode", localeCurrent);
            }
        }
        /// <summary>
        /// Cancel Button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancel_Click(Object sender, EventArgs e)
        {
            ClearFields();
        }
        #endregion

        #region Private members
        /// <summary>
        /// Method to clear all controls on UI
        /// </summary>
        private void ClearFields()
        {
            textCompanyName.Text = string.Empty;
            textEmail.Text = string.Empty;
            textExtension.Text = string.Empty;
            textFax.Text = string.Empty;
            textJobTitle.Text = string.Empty;
            textMessage.Text = string.Empty;
            textMobile.Text = string.Empty;
            textName.Text = string.Empty;
            textPhone.Text = string.Empty;
            selctMainBranch.SelectedIndex = -1;
            selctRequestType.SelectedIndex = 2;
            selctTitle.SelectedIndex = -1;
            selctService.SelectedIndex = -1;
            txtCaptchaCode.Text = string.Empty;
        }
        /// <summary>
        /// Method to set labels over UI
        /// </summary>
        /// <param name="locale"></param>
        private void SetLabelText(uint locale)
        {
            #region Label Text from Resource Files
            lblBranchCity.InnerText = Utility.GetLocalizedValue("elm_Contact_Branch", locale);
            lblCompanyName.InnerText = Utility.GetLocalizedValue("elm_Contact_CompanyName", locale);
            lblEmail.InnerText = Utility.GetLocalizedValue("elm_Contact_Email", locale);
            lblExtension.InnerText = Utility.GetLocalizedValue("elm_Contact_Extension", locale);
            lblFax.InnerText = Utility.GetLocalizedValue("elm_Contact_Fax", locale);
            lblMessage.InnerText = Utility.GetLocalizedValue("elm_Contact_Message", locale);
            lblMobile.InnerText = Utility.GetLocalizedValue("elm_Contact_Mobile", locale);
            lblName.InnerText = Utility.GetLocalizedValue("elm_Contact_Name", locale);
            lblPhone.InnerText = Utility.GetLocalizedValue("elm_Contact_Phone", locale);
            lblRequestType.InnerText = Utility.GetLocalizedValue("elm_Contact_RequestType", locale);
            lblService.InnerText = Utility.GetLocalizedValue("elm_Contact_Service", locale);
            lblJobTitle.InnerText = Utility.GetLocalizedValue("elm_Contact_JobTitle", locale);
            spanCode.InnerText = Utility.GetLocalizedValue("elm_Contact_CaptchaCode", locale);
            spanTitle.InnerText = Utility.GetLocalizedValue("elm_Contact_Title", locale);
            btnSend.Text = Utility.GetLocalizedValue("elm_Contact_SendBtn", locale);
            btnCancel.Text = Utility.GetLocalizedValue("elm_Contact_CancelBtn", locale);
            spanCompletion.InnerText = Utility.GetLocalizedValue("elm_Contact_Completion", locale);
            spanRequiredText.InnerText = Utility.GetLocalizedValue("elm_Contact_RequiredText", locale);
            lblContactText.Text = Utility.GetLocalizedValue("elm_Common_ContactText", locale);
            lblWrongCode.Text = Utility.GetLocalizedValue("elm_Career_Contact_WrongCode", locale);
            #endregion
        }
        /// <summary>
        /// Method to set validation Messages
        /// </summary>
        /// <param name="locale"></param>
        private void SetValidatorMessages(uint locale)
        {
            #region Valiodator Messages from resource Files
            validatorBranch.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Branch", locale);
            validatorBranch.InitialValue = Utility.GetLocalizedValue("elm_Contact_Select_Common", locale);
            validatorCompany.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_CompanyName", locale);
            validatorEmail.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Email", locale);
            validatorEmailRequired.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_EmailRequired", locale);
            validatorMessage.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Message", locale);
            validatorMobile.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Mobile", locale);
            validatorName.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Name", locale);
            validatorPhone.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Phone", locale);
            validatorRequestType.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_RequestType", locale);
            validatorRequestType.InitialValue = Utility.GetLocalizedValue("elm_Contact_Select_Common", locale);
            validatorService.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Service", locale);
            validatorService.InitialValue = Utility.GetLocalizedValue("elm_Contact_Select_Common", locale);
            validatorJobTitle.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_JobTitle", locale);
            validatorCaptcha.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_CaptchaCode", locale);
            validatorTitle.ErrorMessage = Utility.GetLocalizedValue("elm_Contact_Validator_Title", locale);
            validatorTitle.InitialValue = Utility.GetLocalizedValue("elm_Contact_Select_Common", locale);
            #endregion
        }
        /// <summary>
        /// Method to fill dropdown in Page
        /// </summary>
        /// <param name="locale"></param>
        private void FilldropDown(uint locale)
        {
            #region Dropdown Items
            //Title Dropdown
            selctTitle.Items.Insert(0, Utility.GetLocalizedValue("elm_Contact_Select_Common", locale));
            selctTitle.Items.Insert(1, Utility.GetLocalizedValue("elm_Contact_Select_Title_First", locale));
            selctTitle.Items.Insert(2, Utility.GetLocalizedValue("elm_Contact_Select_Title_Second", locale));

            //Branch DropDown
            selctMainBranch.Items.Insert(0, Utility.GetLocalizedValue("elm_Contact_Select_Common", locale));
            selctMainBranch.Items.Insert(1, Utility.GetLocalizedValue("elm_Contact_Select_Branch_First", locale));
            selctMainBranch.Items.Insert(2, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Second", locale));
            selctMainBranch.Items.Insert(3, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Third", locale));
            selctMainBranch.Items.Insert(4, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Fourth", locale));
            selctMainBranch.Items.Insert(5, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Fifth", locale));
            selctMainBranch.Items.Insert(6, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Sixth", locale));
            selctMainBranch.Items.Insert(7, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Seventh", locale));
            selctMainBranch.Items.Insert(8, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Eighth", locale));
            selctMainBranch.Items.Insert(9, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Ninth", locale));
            selctMainBranch.Items.Insert(10, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Tenth", locale));
            selctMainBranch.Items.Insert(11, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Eleventh", locale));
            selctMainBranch.Items.Insert(12, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Twelveth", locale));
            selctMainBranch.Items.Insert(13, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Thirteenth", locale));
            selctMainBranch.Items.Insert(14, Utility.GetLocalizedValue("elm_Contact_Select_Branch_Fourteenth", locale));

            //Request DropDown
            selctRequestType.Items.Insert(0, Utility.GetLocalizedValue("elm_Contact_Select_Common", locale));
            selctRequestType.Items.Insert(1, Utility.GetLocalizedValue("elm_Contact_Select_Request_First", locale));
            selctRequestType.Items.Insert(2, Utility.GetLocalizedValue("elm_Contact_Select_Request_Second", locale));
            selctRequestType.Items.Insert(3, Utility.GetLocalizedValue("elm_Contact_Select_Request_Third", locale));

            //Service DropdDown
            selctService.Items.Insert(0, Utility.GetLocalizedValue("elm_Contact_Select_Common", locale));
            selctService.Items.Insert(1, Utility.GetLocalizedValue("elm_Contact_Select_Service_First", locale));
            selctService.Items.Insert(2, Utility.GetLocalizedValue("elm_Contact_Select_Service_Second", locale));
            selctService.Items.Insert(3, Utility.GetLocalizedValue("elm_Contact_Select_Service_Third", locale));
            selctService.Items.Insert(4, Utility.GetLocalizedValue("elm_Contact_Select_Service_Fourth", locale));
            selctService.Items.Insert(5, Utility.GetLocalizedValue("elm_Contact_Select_Service_Fifth", locale));
            selctService.Items.Insert(6, Utility.GetLocalizedValue("elm_Contact_Select_Service_Sixth", locale));
            selctService.Items.Insert(7, Utility.GetLocalizedValue("elm_Contact_Select_Service_Seventh", locale));
            selctService.Items.Insert(8, Utility.GetLocalizedValue("elm_Contact_Select_Service_Eighth", locale));
            selctService.Items.Insert(9, Utility.GetLocalizedValue("elm_Contact_Select_Service_Ninth", locale));
            selctService.Items.Insert(10, Utility.GetLocalizedValue("elm_Contact_Select_Service_Tenth", locale));
            #endregion
        }
        #endregion
    }
}

