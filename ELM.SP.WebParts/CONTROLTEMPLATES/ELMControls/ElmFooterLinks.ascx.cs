﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using ELM.SP.Library;
namespace ELM.SP.WebParts.ControlTemplates
{
    public partial class ElmFooterLinks : UserControl
    {
         

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        string rootUrl = currentSite.RootWeb.Url;
                        string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                        SPList list = currentWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}", rootUrl, sourceVariation), CoreConstants.Lists.Navigation.URL));


                        SPQuery queryForSideFooter = new SPQuery();
                        queryForSideFooter.Query = string.Format("<Where><Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq></Where><OrderBy><FieldRef Name='{2}' Ascending='TRUE' /></OrderBy>",CoreConstants.Lists.Navigation.Fields.NavigationContentType, CoreConstants.Lists.Navigation.SideFooterContentType, CoreConstants.Lists.Navigation.Fields.NavigationDisplayOrder);
                        SPListItemCollection listOfItemsForSideFooter = list.GetItems(queryForSideFooter);
                        repFooterRight.DataSource = listOfItemsForSideFooter.GetDataTable();
                        repFooterRight.DataBind();


                        SPQuery queryforCenterFooter = new SPQuery();
                        queryforCenterFooter.Query = string.Format("<Where><Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq></Where><OrderBy><FieldRef Name='{2}' Ascending='TRUE' /></OrderBy>", CoreConstants.Lists.Navigation.Fields.NavigationContentType, CoreConstants.Lists.Navigation.CenterFooterContentType, CoreConstants.Lists.Navigation.Fields.NavigationDisplayOrder);
                        SPListItemCollection listOfItemsForCenterFooter = list.GetItems(queryforCenterFooter);
                        repFooterCenter.DataSource = listOfItemsForCenterFooter.GetDataTable();
                        repFooterCenter.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        /// <summary>
        /// Event to bind right portion of footer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repNavigationRightFooterAdded_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.ItemType == ListItemType.Header)
            {
                ((Label)e.Item.FindControl("lblFooterRight")).Text = Utility.GetLocalizedValue("Elm_Navigation_RightFooter_Header", locale);
            }
        }
        /// <summary>
        /// Event to bind Center portion of footer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repNavigationCenterFooterAdded_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.ItemType == ListItemType.Header)
            {
                ((Label)e.Item.FindControl("lblFooterCenter")).Text =Utility.GetLocalizedValue("Elm_Navigation_CenterFooter_Header", locale);
            }
        }
        #endregion
    }
}
