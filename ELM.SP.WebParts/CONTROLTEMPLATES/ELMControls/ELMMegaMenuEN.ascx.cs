﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Text;
using Microsoft.SharePoint.Publishing.Fields;
using ELM.SP.Library;
namespace ELM.SP.WebParts.CONTROLTEMPLATES
{
    public partial class ELMMegaMenuEN : UserControl
    {
        #region Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            GetMegaMenu();
        }
        #endregion

        #region Private members
        /// <summary>
        /// Method to get MegaMenu and append it at prescribed tab
        /// </summary>
        private void GetMegaMenu()
        {
            try
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                {
                    string completePageUrl = Convert.ToString(SPContext.Current.RootFolderUrl);
                    int lastIndex = completePageUrl.LastIndexOf('/');
                    string PageUrl = currentSite.Url + completePageUrl.Substring(0, lastIndex).ToLower();

                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        StringBuilder megaMenu = null;
                        megaMenu = GetMegaMenuString(currentSite, currentWeb, PageUrl);
                        megaMenuDiv.InnerHtml = megaMenu.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        /// <summary>
        /// Method to build string to be appended for mega menu
        /// </summary>
        /// <param name="currentSite"></param>
        /// <param name="currentWeb"></param>
        /// <param name="PageUrl"></param>
        /// <returns></returns>
        private StringBuilder GetMegaMenuString(SPSite currentSite, SPWeb currentWeb, string PageUrl)
        {
            StringBuilder megaMenu = new StringBuilder();
            try 
            {
            string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
            string rootUrl = string.Format("{0}/{1}", currentSite.RootWeb.Url, sourceVariation);
            megaMenu.Append("<ul class='mainMenu megamenu'>");
            SPList list = currentWeb.GetList(SPUrlUtility.CombineUrl(rootUrl, CoreConstants.Lists.Navigation.URL));

            SPQuery query = new SPQuery();
            query.Query = string.Format("<Where><Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq></Where><OrderBy><FieldRef Name='{2}' Ascending='TRUE' /></OrderBy>", CoreConstants.Lists.Navigation.Fields.NavigationContentType, CoreConstants.Lists.Navigation.MenuNavigationContentType, CoreConstants.Lists.Navigation.Fields.NavigationDisplayOrder);
            SPListItemCollection listOfItemsForTopMenu = list.GetItems(query);

            if (listOfItemsForTopMenu.Count > 0)
            {
                foreach (SPListItem item in listOfItemsForTopMenu)
                {
                    if (Convert.ToString(item[CoreConstants.Lists.Navigation.Fields.NavigationRedirectURL]).Split(',')[0].ToLower().Equals(PageUrl))
                    {
                        megaMenu.AppendFormat("<li class='menuItemSelected'><a href=\'{0}\' class='megamenu_drop'>{1}</a>", Convert.ToString(item[CoreConstants.Lists.Navigation.Fields.NavigationRedirectURL]).Split(',')[0], item[CoreConstants.Lists.Navigation.Fields.NavigationTitle]);
                    }
                    else
                    {
                        megaMenu.AppendFormat("<li><a href=\'{0}\' class='megamenu_drop'>{1}</a>", Convert.ToString(item[CoreConstants.Lists.Navigation.Fields.NavigationRedirectURL]).Split(',')[0], item[CoreConstants.Lists.Navigation.Fields.NavigationTitle]);
                    }

                    SPQuery subChildQuery = new SPQuery();
                    subChildQuery.Query = string.Format("<Where><And><Eq><FieldRef Name='{0}' /><Value Type='Text'>{1}</Value></Eq><Eq><FieldRef Name='{3}' LookupId='TRUE' /><Value Type='Lookup'>{4}</Value></Eq></And></Where><OrderBy><FieldRef Name='{2}' Ascending='TRUE' /></OrderBy>", CoreConstants.Lists.Navigation.Fields.NavigationContentType, CoreConstants.Lists.Navigation.SubMenuNavigationContentType, CoreConstants.Lists.Navigation.Fields.NavigationDisplayOrder,CoreConstants.Lists.Navigation.Fields.NavigationSubHeadingParent,item.ID);
                    SPListItemCollection subChildCollections = list.GetItems(subChildQuery);

                    if (subChildCollections.Count > 0)
                    {
                        megaMenu.Append("<div style='left: -1px; top: auto;' class='dropdown_container'>");
                        foreach (SPListItem subChild in subChildCollections)
                        {
                            ImageFieldValue loc = subChild[CoreConstants.Lists.Navigation.Fields.NavigationImage] as ImageFieldValue;

                            megaMenu.Append("<div class='col_6'>");
                            megaMenu.AppendFormat("<img src=\'{0}\'></img>", loc.ImageUrl.Replace(" ", "%20"));
                            megaMenu.AppendFormat(" <a href=\'{0}\'> <h3>{1}</h3></a><p>{2}</p>", Convert.ToString(subChild[CoreConstants.Lists.Navigation.Fields.NavigationRedirectURL]).Split(',')[0], subChild[CoreConstants.Lists.Navigation.Fields.NavigationTitle], subChild[CoreConstants.Lists.Navigation.Fields.NavigationSubHeadingDescription]);
                            megaMenu.AppendFormat("<a href = \'{0}\'><span class='menuArrow'></span></a>", Convert.ToString(subChild[CoreConstants.Lists.Navigation.Fields.NavigationRedirectURL]).Split(',')[0]);
                            megaMenu.Append("</div>");
                        }
                        megaMenu.Append("</div>");
                    }
                    megaMenu.Append("</li>");
                }
            }
            megaMenu.Append("</ul>");  
            }
            catch (Exception ex) 
            {
                LoggerService.LogError(LoggerService.LOG_ERROR,string.Format("Exception Caught - {0}",ex.Message));
            }
            return megaMenu;
        }
        #endregion
    }
}
