﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ELMMegaMenuEN.ascx.cs" Inherits="ELM.SP.WebParts.CONTROLTEMPLATES.ELMMegaMenuEN" %>

<link rel="stylesheet" href="/_layouts/ELM/MegaMenu/megamenu.css" />
<script type="text/javascript" src="/_layouts/ELM/MegaMenu/megamenu_plugins.js"></script>
<script type="text/javascript" src="/_layouts/ELM/MegaMenu/megamenu.js"></script>

<script type="text/javascript">
    $(document).ready(function ($) {
        $('.megamenu').megaMenuCompleteSet({
            menu_speed_show: 100, // Time (in milliseconds) to show a drop down
            menu_speed_hide: 100, // Time (in milliseconds) to hide a drop down
            menu_speed_delay: 100, // Time (in milliseconds) before showing a drop down
            menu_effect: 'hover_fade', // Drop down effect, choose between 'hover_fade', 'hover_slide', etc.
            menu_click_outside: 1, // Clicks outside the drop down close it (1 = true, 0 = false)
            menu_show_onload: 0, // Drop down to show on page load (type the number of the drop down, 0 for none)
            menu_responsive: 1 // 1 = Responsive, 0 = Not responsive
        });
    });
</script>




<div class="menu-container">
	        <div runat="server" id="megaMenuDiv" class="wrapper">
    </div>
</div>