﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElmSubscription.ascx.cs"
    Inherits="ELM.SP.WebParts.CONTROLTEMPLATES.ElmSubscription" %>
<asp:UpdatePanel ID="upnlSUbscription" runat="server">
    <ContentTemplate>
        <div class="subscriptionText">
            <asp:Label ID="lbltext" runat="server"></asp:Label></div>
        <asp:Panel ID="pnlSubscription" runat="server">
            <div class="subscriptionInput">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="textForEmail"></asp:TextBox>
                <asp:RequiredFieldValidator ID="validatorEmailRequired" runat="server" ControlToValidate="txtEmail"
                    Display="Dynamic" ValidationGroup="RegistrationGroup"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="validatorEmail" ControlToValidate="txtEmail"
                    Display="Dynamic" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    ValidationGroup="RegistrationGroup"></asp:RegularExpressionValidator>
            </div>
            <asp:Panel ID="pnlSubscribed" runat="server" Visible="false">
                <div class="subscribedMessage">
                    <asp:Label ID="lblSubscribed" runat="server"></asp:Label></div>
            </asp:Panel>
            <asp:Panel ID="pnlSubscriptionError" runat="server" Visible="false">
                <div class="subscriptionErrorMessage">
                    <asp:Label ID="lblSubscriptionError" runat="server"></asp:Label></div>
            </asp:Panel>
            <div class="subscriptionRegister">
                <asp:Button ID="btnRegister" runat="server" ValidationGroup="RegistrationGroup" OnClick="BtnRegister_Click" />
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
