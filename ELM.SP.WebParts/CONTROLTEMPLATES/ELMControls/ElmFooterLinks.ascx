﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElmFooterLinks.ascx.cs"
    Inherits="ELM.SP.WebParts.ControlTemplates.ElmFooterLinks" %>
<asp:Repeater ID="repFooterRight" runat="server" OnItemDataBound="repNavigationRightFooterAdded_OnItemDataBound">
    <HeaderTemplate>
        <div class="footerTopRight">
            <h6>
                <asp:Label ID="lblFooterRight" runat="server"></asp:Label></h6>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><a href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
            <%# Eval("Title")%></a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
<asp:Repeater ID="repFooterCenter" runat="server" OnItemDataBound="repNavigationCenterFooterAdded_OnItemDataBound">
    <HeaderTemplate>
        <div class="footerTopCenter">
            <h6>
                <asp:Label ID="lblFooterCenter" runat="server"></asp:Label></h6>
            <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><a href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
            <%# Eval("Title")%></a></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul> </div>
    </FooterTemplate>
</asp:Repeater>
