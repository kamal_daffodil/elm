﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using ELM.SP.Library;
namespace ELM.SP.WebParts.ControlTemplates
{
    public partial class ElmStaticSociaLinks : UserControl
    {
        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            GetSocialLinks();
        }
        #endregion

        #region Private Members
        /// <summary>
        /// Method to get social links to be displayed in header of master page
        /// </summary>
        private void GetSocialLinks() 
        {
            try
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                        string rootUrl = string.Format("{0}/{1}", currentSite.RootWeb.Url, sourceVariation);

                        SPList socialMedia = currentWeb.GetList(SPUrlUtility.CombineUrl(rootUrl, CoreConstants.Lists.SocialAccounts.URL));

                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy>", CoreConstants.Lists.SocialAccounts.Fields.AccountDisplayOrder);
                        SPListItemCollection socialMediaItems = socialMedia.GetItems(query);
                        repSocialStaticLinks.DataSource = socialMediaItems.GetDataTable();
                        repSocialStaticLinks.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        #endregion
    }
}
