﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using ELM.SP.Library;
namespace ELM.SP.WebParts.CONTROLTEMPLATES
{
    public partial class ElmSubscription : UserControl
    {
         
        #region Events
        /// <summary>
        /// Page load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                pnlSubscription.Visible = false;
            }
            else
            {
                uint locale = (uint)SPContext.Current.Web.Locale.LCID;
                // set label text from resource files
                SetLabelText(locale);
                //set validator messages from resource files
                SetValidatorMessages(locale);
            }
        }
        /// <summary>
        /// Click Event to register user for NewsLetter Subscription
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnRegister_Click(Object sender, EventArgs e)
        {
            pnlSubscribed.Visible = false;
            pnlSubscriptionError.Visible = false;
            try
            {
                Guid siteId = SPContext.Current.Web.Site.ID;
                SubscribeUser(siteId);
            }
            catch(Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        #endregion

        #region Private Members
        /// <summary>
        /// method will add email subscribed to Subscription List
        /// </summary>
        /// <param name="siteId"></param>
        private void SubscribeUser(Guid siteId) 
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                using (SPSite currentSite = new SPSite(siteId))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb(string.Format("/{0}", sourceVariation)))
                    {
                        string emailTextFromUser = txtEmail.Text.Trim();
                        string rootUrl = currentSite.RootWeb.Url;
                        SPList subscriptionList = currentWeb.Lists[CoreConstants.Lists.Subscription.URL];
                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<Where><Eq><FieldRef Name=\"{0}\"/><Value Type=\"Text\">{1}</Value></Eq></Where>", CoreConstants.Lists.Subscription.Fields.SubscriptionEMail, emailTextFromUser);
                        SPListItemCollection subscriptionItems = subscriptionList.GetItems(query);
                        if (subscriptionItems.Count > 0)
                        {
                            txtEmail.Text = string.Empty;
                            pnlSubscriptionError.Visible = true;
                        }
                        else
                        {
                            SPListItem emailToAddInList = subscriptionItems.Add();
                            emailToAddInList[CoreConstants.Lists.Subscription.Fields.SubscriptionEMail] = emailTextFromUser;
                            currentWeb.AllowUnsafeUpdates = true;
                            emailToAddInList.Update();
                            currentWeb.AllowUnsafeUpdates = false;
                            pnlSubscribed.Visible = true;
                        }
                        txtEmail.Text = string.Empty;
                    }
                }
            });
        }
        /// <summary>
        /// method to set labels to be used by control displayed in the footer
        /// </summary>
        /// <param name="locale"></param>
        private void SetLabelText(uint locale) 
        {
            #region Setting text over labels, buttons and inputs from resource files
            lblSubscribed.Text = Utility.GetLocalizedValue("elm_Subscription_SubscribedMessage", locale);
            lblSubscriptionError.Text = Utility.GetLocalizedValue("elm_Subscription_ErrorMessage", locale);
            lbltext.Text = Utility.GetLocalizedValue("elm_Subscription_Title", locale);
            btnRegister.Text = Utility.GetLocalizedValue("elm_Subscription_RegisterButton", locale);
            txtEmail.Attributes.Add("placeholder", Utility.GetLocalizedValue("elm_Subscription_EmailField", locale));
            #endregion
        }
        /// <summary>
        /// method to set validation Messages
        /// </summary>
        /// <param name="locale"></param>
        private void SetValidatorMessages(uint locale) 
        {
            #region Setting error messages of validators
            validatorEmailRequired.ErrorMessage = Utility.GetLocalizedValue("elm_Subscription_EmailRequired", locale);
            validatorEmail.ErrorMessage = Utility.GetLocalizedValue("elm_Subscription_EmailInvalid", locale);
            #endregion
        }
        #endregion
    }
}
