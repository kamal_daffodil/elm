﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Assembly Name="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="CMS" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"
    Namespace="Microsoft.SharePoint.Publishing.WebControls" %>

<CMS:VariationDataSource ID="LabelMenuDataSource" LabelMenuConfiguration="1" Filter=""
    runat="server" />
<asp:Repeater ID="Repeater1" runat="server" DataSourceID="LabelMenuDataSource" EnableViewState="False"> 
  <ItemTemplate>
    <a href="<%# DataBinder.Eval(Container.DataItem, "NavigateUrl") %>">
    <%# DataBinder.Eval(Container.DataItem, "DisplayText") %></a> 
  </ItemTemplate>
  <SeparatorTemplate> | </SeparatorTemplate>
</asp:Repeater>