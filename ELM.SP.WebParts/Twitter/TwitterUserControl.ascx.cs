﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint;
using ELM.SP.Library;
namespace ELM.SP.WebParts.Twitter
{
    public partial class TwitterUserControl : UserControl
    {
        #region Properties
        public Twitter oTwitterWebpart { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            string redirectUrl = Convert.ToString(oTwitterWebpart._SourceUri);
            if (string.IsNullOrEmpty(redirectUrl))
            {
                lnkFollow.NavigateUrl = CoreConstants.Lists.TwitterFeeds.ELMFollowLink;
            }
            else
            {
                lnkFollow.NavigateUrl = redirectUrl;
            }
            lnkFollow.Text = Utility.GetLocalizedValue("Twitter_Followus", locale);
        }
        #endregion
    }
}
