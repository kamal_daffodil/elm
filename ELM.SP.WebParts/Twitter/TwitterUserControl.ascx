﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwitterUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.Twitter.TwitterUserControl" %>
<script src="/_layouts/ELM/Marquee/jquery.marquee.min.js" type="text/javascript"></script>
<script src="/_layouts/ELM/Marquee/jquery.pause.min.js" type="text/javascript"></script>

<style type="text/css">
    .marquee
    {
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    String.prototype.parseURL = function () {
        return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function (url) {
            return url.link(url);
        });
    };
    String.prototype.parseHashtag = function () {
        return this.replace(/[#]+[A-Za-z0-9-_]+/g, function (t) {
            var tag = t.replace("#", "%23")
            return t.link("http://search.twitter.com/search?q=" + tag);
        });
    };

    String.prototype.parseUsername = function () {
        return this.replace(/[@]+[A-Za-z0-9-_]+/g, function (u) {
            var username = u.replace("@", "")
            return u.link("http://twitter.com/" + username);
        });
    };
    $(document).ready(function () {

        var pathArray = window.location.href.split('/');

        var defaults = {
            limit: 5,
            username: 'bitscape_global',
            time: false,
            replies: false,
            position: 'append'
        };

        var act = $(this);
        var $tweetList;
        var tweetMonth = '';
        $.ajax({
            url: "/" + pathArray[3] + "/_layouts/ELM/TwitterHandler/twitterfeed.ashx",
            contentType: 'application/json',
            success: function (data, z) {

                $.each($.parseJSON(data), function (i, item) {
                    if (i == 0) {
                        $tweetList = $('<p></p>');
                    }

                    if (defaults.replies === false) {

                        if (item.in_reply_to_status_id === null) {
                            $tweetList.append('<div class="twitlinks">' + item.text.parseURL().parseHashtag().parseUsername() + '</div>');
                        }
                        else {
                            $tweetList.append('<div class="twitlinks">' + item.text.parseURL().parseHashtag().parseUsername() + '</div>');
                        }

                    }

                    if (defaults.time == true) {
                        for (var iterate = 0; iterate <= 12; iterate++) {
                            if (shortMonths[iterate] == item.created_at.substr(4, 3)) {
                                tweetMonth = iterate + 1;
                                if (tweetMonth < 10) {
                                    tweetMonth = '0' + tweetMonth;
                                }
                            }
                        }
                    }
                });

                $('#tweets').html($tweetList[0].outerHTML);
                var msVersion = navigator.userAgent.match(/MSIE ([0-9]{1,}[\.0-9]{0,})/),
                msie = !!msVersion,
                ie8 = msie && parseFloat(msVersion[1]) < 9;

                $('.marquee').marquee({
                    pauseOnHover: true
                });

                $('.marquee').hover(function () {
                    $('.marquee').pause();
                });

            },
            error: function (data, z, x) {

            }
        });
    });
</script>
<div class="twitterBlock">
    <div class="twitHeading">
        <asp:HyperLink runat="server" ID="lnkFollow" Target="_blank"></asp:HyperLink>
        </div>
    <div id="tweets" class="marquee ver" data-direction='up' data-duration="6000">
    </div>
</div>
