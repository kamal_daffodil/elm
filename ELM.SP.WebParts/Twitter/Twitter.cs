﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace ELM.SP.WebParts.Twitter
{
    [ToolboxItemAttribute(false)]
    public class Twitter : WebPart
    {
        [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Follow us link"),
        WebDescription("Please Enter a Sample Text")]
        public string _SourceUri { get; set; }

        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/ELM.SP.WebParts/Twitter/TwitterUserControl.ascx";

        protected override void CreateChildControls()
        {
            TwitterUserControl twitterControl = (TwitterUserControl)Page.LoadControl(_ascxPath);
            if (twitterControl != null)
            {
                twitterControl.oTwitterWebpart = this;
                Controls.Add(twitterControl);
            }
        }
    }
}
