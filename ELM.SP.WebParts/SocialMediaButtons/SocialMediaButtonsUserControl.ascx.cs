﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace ELM.SP.WebParts.SocialMediaButtons
{
    public partial class SocialMediaButtonsUserControl : UserControl
    {
        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url)) 
            {
                using (SPWeb currentWeb = currentSite.OpenWeb())
                {
                    string variation = SPContext.Current.Web.Locale.Parent.Name;
                    if (variation == "ar")
                    {
                        englishButtons.Visible = false;
                        arabicButtons.Visible = true;
                    }
                    else 
                    {
                        englishButtons.Visible = true;
                        arabicButtons.Visible = false;
                    }       
                }
            }
        }
        #endregion
    }
}
