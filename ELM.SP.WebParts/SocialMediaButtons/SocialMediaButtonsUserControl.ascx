﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SocialMediaButtonsUserControl.ascx.cs" Inherits="ELM.SP.WebParts.SocialMediaButtons.SocialMediaButtonsUserControl" %>

<style type="text/css">
.socialBlock{
visibility: hidden;
margin:6px;
}
</style>

<%--<script type="text/javascript">
    window.fbAsyncInit = function () {
        FB.init({
            appId: '662783300504199',
            xfbml: true,
            version: 'v2.1'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));</script>

    <script type="text/javascript">        !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); } } (document, 'script', 'twitter-wjs');</script>

--%>
<asp:Label ID="englishButtons" runat="server" Visible="false" style="display:none; visibility:hidden">
<div class="fb-like" data-href="https://www.facebook.com/AlElmCo" data-width="190"  data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" data-size="large" ></div>
<div class="tweetBlock"><a href="http://twitter.com/share" class="twitter-share-button" data-dnt="true" data-count="horizontal" data-via="elm" data-lang="en">Tweet</a></div>
</asp:Label>

<asp:Label ID="arabicButtons" runat="server" Visible="false" style="display:none; visibility:hidden">
<div class="fb-like" data-href="https://www.facebook.com/AlElmCo" data-width="190"  data-layout="button_count" data-action="like" data-show-faces="false" data-share="false" data-size="large"></div>
<div class="tweetBlock"><a href="http://twitter.com/share" class="twitter-share-button" data-dnt="true" data-count="horizontal" data-via="elm" data-lang="ar">Tweet</a></div>
</asp:Label>
