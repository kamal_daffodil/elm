﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ELM.SP.Library;
namespace ELM.SP.WebParts.Services
{
    public partial class ServicesUserControl : UserControl
    {
         
        #region Events
        /// <summary>
        /// Page load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;

            lblserviceLabel.Text = Utility.GetLocalizedValue("elm_Services_Label", locale);
            try
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        string rootUrl = currentSite.RootWeb.Url;
                        string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                        SPList eServices = currentWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}", rootUrl, sourceVariation), CoreConstants.Lists.SubService.URL));

                        SPQuery query = new SPQuery();
                        query.Query = string.Format( "<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy>",CoreConstants.Lists.SubService.Fields.ServiceDisplayOrder);

                        SPListItemCollection servicesListItems = eServices.GetItems(query);
                        repServices.DataSource = servicesListItems;
                        repServices.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception-{0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }

        }
        /// <summary>
        /// Repeater Bind event for Services under service site
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ServiceOnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.DataItem != null)
            {
                ((Label)e.Item.FindControl("lblMore")).Text = Utility.GetLocalizedValue("elm_Service_MoreUrl", locale);
            }
        }
        #endregion

        #region Public members
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="htmlText"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(string htmlText)
        {
            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// Method to get substring from the data obtained from List Field
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {

            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }

            var descriptionText = string.Empty;

            if (!String.IsNullOrEmpty(itemDataString))
            {

                descriptionText = itemDataString.Length > 120 ? string.Format("{0}...", itemDataString.Substring(0, 120)) : itemDataString;


            }

            return descriptionText;
        }
        /// <summary>
        /// Method to get image from handler and bind to design
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetImageurl(object itemData)
        {
            string rootUrl = SPContext.Current.Site.Url;
            Microsoft.SharePoint.Publishing.Fields.ImageFieldValue imageUrl = (Microsoft.SharePoint.Publishing.Fields.ImageFieldValue)itemData;
            string ImageUrl = imageUrl.ImageUrl;
            string pictureUrl = string.Empty;
            if (ImageUrl.Contains("http://"))
            {
                pictureUrl = string.Format("{0}2&imageUrl={1}", CoreConstants.ImageHandlerURL, ImageUrl);
            }
            else
            {
                pictureUrl = string.Format("{0}2&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, ImageUrl);
            }
            return pictureUrl;
        }
        #endregion
    }
}
