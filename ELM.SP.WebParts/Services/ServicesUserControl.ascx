﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicesUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.Services.ServicesUserControl" %>

<link href="/_layouts/ELM/eServices/component.css" rel="stylesheet" />
<link href="/_layouts/ELM/eServices/default.css" rel="stylesheet" />
<script src="/_layouts/ELM/eServices/modernizr.custom.js" type="text/javascript"></script>
<script src="/_layouts/ELM/eServices/toucheffects.js" type="text/javascript"></script>

<div class="categorySolutions">
    <asp:Label runat="server" CssClass="serviceLabel" ID="lblserviceLabel" Text="المنتجات"></asp:Label>
    <asp:Repeater ID="repServices" runat="server" OnItemDataBound="ServiceOnItemDataBound">
        <HeaderTemplate>
            <div class="container demo-3">
                <ul class="grid cs-style-3">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <figure>
            <div class="catSolimg">
                <a href='<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(',')[0] %>'>
                    <asp:Image ID="imgServices" ImageUrl='<%# GetImageurl(((SPListItem)Container.DataItem)["ELMPublishingImage"]) %>' runat="server" /> 
                </a>
            </div>
            <figcaption style="height:96px">
            <div class="catSoldes">
              <a href='<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(',')[0] %>'>   <h4 class="lblMore" style="color:#fff"><asp:Label ID="lblMore" runat="server"></asp:Label> </h4> </a>
                <span class="catesoltext innerservicePage">
                    <%# GetDescriptionText(((SPListItem)Container.DataItem)["ELMDescription"])%></span>
            </div>
            </figcaption>
        </figure>
                <a href='<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(',')[0] %>'>
                    <h4 class="innerPageServiceTitle" style="color: <%# ((SPListItem)Container.DataItem)["ELMTitleColorCode"] %>">
                        <%# ((SPListItem)Container.DataItem)["Title"]%>
                    </h4>
                </a></li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div class="clr">
</div>
