﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SolutionsUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.Solutions.SolutionsUserControl" %>
<h4 id="emptyListingText" runat="server">
</h4>
<div class="categorySolutions solutionPage" style="float: left; display: inline-block">
    <asp:Label runat="server" CssClass="solutionLabel" ID="lblsolutionLabel" Text="الحلول"></asp:Label>
    <asp:Repeater ID="repSolutionsCategory" runat="server" OnItemDataBound="repSolutionsCategory_OnItemDataBound">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <div class="catSoldes">
                    <h4>
                        <a href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
                            <%# Eval("Title")%>
                            <%# Eval("ELMPublishingIcon")%>
                        </a>
                    </h4>
                    <span class="catesoltext">
                        <%# GetDescriptionText(Eval("ELMDescription"))%>
                    </span><a class="readMore" href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
                        <asp:Label ID="lblMore" runat="server"></asp:Label>
                    </a>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>
<div class="clr">
</div>
