﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Text.RegularExpressions;
using ELM.SP.Library;

namespace ELM.SP.WebParts.Solutions
{
    public partial class SolutionsUserControl : UserControl
    {
         
        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            emptyListingText.InnerText = Utility.GetLocalizedValue("elm_Solutions_NoRecordMessage", locale);
            lblsolutionLabel.Text = Utility.GetLocalizedValue("elm_Solution_Label", locale);
            try
            {
                using (SPSite spSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        string rootUrl = spSite.RootWeb.Url;
                        string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                        emptyListingText.Visible = false;
                        SPList eServices = spWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}", rootUrl, sourceVariation), CoreConstants.Lists.Solution.URL));
                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy>",CoreConstants.Lists.Solution.Fields.SolutionsDisplayOrder);
                        SPListItemCollection items = eServices.GetItems(query);
                        if (items.Count > 0)
                        {
                            DataTable eServiceDataTable = items.GetDataTable();
                            repSolutionsCategory.DataSource = eServiceDataTable;
                            repSolutionsCategory.DataBind();
                        }
                        else
                        {
                            emptyListingText.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        /// <summary>
        /// Repeater Data Bound event for Solutions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repSolutionsCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.DataItem != null)
            {
                ((Label)e.Item.FindControl("lblMore")).Text = Utility.GetLocalizedValue("elm_NewsGallery_MoreUrl", locale);
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="htmlText"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(string htmlText)
        {
            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// Method to get substring from the data obtained from List Field
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {
            var descriptionText = string.Empty;

            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }

                if (!String.IsNullOrEmpty(itemDataString))
                {
                    descriptionText = itemDataString.Length > 250 ? string.Format("{0}...", itemDataString.Substring(0, 250)) : itemDataString;
                }

            return descriptionText;
        }
        #endregion
    }
}

