﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Drawing.Imaging;
using System.Net;
using System.Text.RegularExpressions;
using ELM.SP.Library;

namespace ELM.SP.WebParts.NewsGallery
{
    public partial class NewsGalleryUserControl : UserControl
    {
        #region properties
        public NewsGallery redirectUrl { get; set; }
        #endregion

         

        #region Variables
        PagedDataSource pgsource;
        int lindex, findex;
        #endregion

        #region Events
        /// <summary>
        /// Page  load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataList();
            }

        }
        /// <summary>
        /// Repeater Bind Event to bind all news in gallery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void newsGalleryRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            Label lblReadMore = ((Label)e.Item.FindControl("lblReadMore"));
            lblReadMore.Text = Utility.GetLocalizedValue("elm_NewsGallery_MoreUrl", locale);
            string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
            if (e.Item.DataItem != null)
            {
                ((Label)e.Item.FindControl("lblReadMore")).Text = Utility.GetLocalizedValue("elm_NewsGallery_MoreUrl", locale);

                string itemId = Convert.ToString(((HtmlInputHidden)e.Item.FindControl("hdnNewsCategoryID")).Value);

                if (string.IsNullOrEmpty(redirectUrl._newsDetailUrl))
                {
                    ((HtmlAnchor)e.Item.FindControl("A1")).HRef = string.Format("/{0}/News/Pages/{1}" , sourceVariation , itemId);
                    ((HtmlAnchor)e.Item.FindControl("lnkReadMore")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                }
                else
                {
                    ((HtmlAnchor)e.Item.FindControl("A1")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                    ((HtmlAnchor)e.Item.FindControl("lnkReadMore")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                }
            }

        }
        #endregion

        #region Public Members
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="htmlText"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(string htmlText)
        {
            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// Method to get substring from the data obtained from List Field
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {
            string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }

            var descriptionText = string.Empty;

            if (!String.IsNullOrEmpty(itemDataString))
            {
                if (sourceVariation == "ar")
                {
                    descriptionText = itemDataString.Length > 180 ? string.Format("{0}...", itemDataString.Substring(0, 180)) : itemDataString;
                }
                else
                {
                    descriptionText = itemDataString.Length > 180 ? string.Format("{0}...", itemDataString.Substring(0, 170)) : itemDataString;
                }

            }

            return descriptionText;
        }
        /// <summary>
        /// Method to get image from handler and bind to design
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetImageurl(object itemData)
        {
            string cultureName = SPContext.Current.Web.Locale.Parent.Name;
            string rootUrl = SPContext.Current.Site.Url;
            string pictureUrl = string.Empty;
            try
            {
                
                string hiddenFieldValue = Convert.ToString(itemData);
                int srcIndex = hiddenFieldValue.IndexOf("src=");
                int startIndex = hiddenFieldValue.IndexOf('"', srcIndex) + 1;
                int endIndex = hiddenFieldValue.IndexOf('"', startIndex) - startIndex;
                string imageUrl = hiddenFieldValue.Substring(startIndex, endIndex);

                if (imageUrl.Length > 1)
                {
                    if (imageUrl.Contains("http://"))
                    {
                        pictureUrl = string.Format("{0}3&imageUrl={1}", CoreConstants.ImageHandlerURL, imageUrl);
                    }
                    else
                    {
                        pictureUrl = string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, imageUrl);
                    }
                }
                else
                {
                    pictureUrl = (cultureName == "ar") ? string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsArabicImageURL) : string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsEnglishImageURL);
                }
            }
            catch (Exception ex) 
            {
                LoggerService.LogError(LoggerService.LOG_ERROR, string.Format("Exception Caught - {0}",ex.Message));
                pictureUrl = (cultureName == "ar") ? string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL,rootUrl, CoreConstants.Lists.News.DefaultNewsArabicImageURL) : string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL,rootUrl, CoreConstants.Lists.News.DefaultNewsEnglishImageURL);
            }
            return pictureUrl;
        }
        /// <summary>
        /// Method to get input as a string and convert it into arabic Language to be displayed in Arabic Variation
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string ConvertToArabicNumerals(string input)
        {
            System.Text.UTF8Encoding utf8Encoder = new UTF8Encoding();
            System.Text.Decoder utf8Decoder = utf8Encoder.GetDecoder();
            System.Text.StringBuilder convertedChars = new System.Text.StringBuilder();
            char[] convertedChar = new char[1];
            byte[] bytes = new byte[] { 217, 160 };
            char[] inputCharArray = input.ToCharArray();
            foreach (char c in inputCharArray)
            {
                if (char.IsDigit(c))
                {
                    bytes[1] = Convert.ToByte(160 + char.GetNumericValue(c));
                    utf8Decoder.GetChars(bytes, 0, 2, convertedChar, 0);
                    convertedChars.Append(convertedChar[0]);
                }
                else
                {
                    convertedChars.Append(c);
                }
            }
            return convertedChars.ToString();
        }
        #endregion        

        #region Private Members and properties
        private int m_GetCurrentPageNumber = 0;
        private int GetCurrentPageNumber
        {
            get
            {
                if (ViewState["CurrentPage"] != null)
                {
                    return Convert.ToInt32(ViewState["CurrentPage"]);
                }
                else
                {
                    return m_GetCurrentPageNumber;
                }
            }
            set
            {
                ViewState["CurrentPage"] = value;
            }
        }
        #endregion

        #region "Pagination"

        #region Private Members
        /// <summary>
        /// Method to bind data to repeater for news gallery webpart
        /// </summary>
        private void BindDataList()
        {
            //Create new DataTable dt
            pgsource = new PagedDataSource();
            DataTable dt = GetData();
            if (dt != null && dt.Rows.Count > 0)
            {
                pgsource.DataSource = dt.DefaultView;
                //Set PageDataSource paging 
                pgsource.AllowPaging = true;
                //Set number of items to be displayed in the Repeater using drop down list
                pgsource.PageSize = 4;
                //Get Current Page Index
                pgsource.CurrentPageIndex = CurrentPage;
                //Store it Total pages value in View state
                ViewState["totpage"] = pgsource.PageCount;
                lnkPrevious.Visible = lnkPrevious.Enabled = !pgsource.IsFirstPage;
                //Enabled true Link button Next when current page is not equal last page 
                //Enabled false Link button Next when current page is last page
                lnkNext.Visible = lnkNext.Enabled = !pgsource.IsLastPage;
                //Bind resulted PageSource into the Repeater
                newsGalleryRepeater.DataSource = pgsource;
                newsGalleryRepeater.DataBind();
                lblNoNewsFound.Visible = false;
                //Create Paging with help of DataList control "RepeaterPaging"
                doPaging();
                RepeaterPaging.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            }
            else
            {
                lblNoNewsFound.Visible = true;
                string cultureName = SPContext.Current.Web.Locale.Parent.Name;
                lblNoNewsFound.Text = (cultureName == "ar") ? "لا يوجد أخبار وجدت" : "No News Found";
                lnkPrevious.Visible = lnkNext.Visible = false;
                //Enabled true Link button Next when current page is not equal last page 
                //Enabled false Link button Next when current page is last page
                newsGalleryRepeater.DataSource = null;
                newsGalleryRepeater.DataBind();
                RepeaterPaging.DataSource = null;
                RepeaterPaging.DataBind();
            }

        }
        /// <summary>
        /// Method to perform paging and bind to repeater made for paging
        /// </summary>
        private void doPaging()
        {
            DataTable dt = new DataTable();
            //Add two column into the DataTable "dt" 
            //First Column store page index default it start from "0"
            //Second Column store page index default it start from "1"
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");

            //Assign First Index starts from which number in paging data list
            findex = CurrentPage - 5;

            //Set Last index value if current page less than 5 then last index added "5" values to the Current page else it set "10" for last page number
            if (CurrentPage > 5)
            {
                lindex = CurrentPage + 5;
            }
            else
            {
                lindex = 10;
            }

            //Check last page is greater than total page then reduced it to total no. of page is last index
            if (lindex > Convert.ToInt32(ViewState["totpage"]))
            {
                lindex = Convert.ToInt32(ViewState["totpage"]);
                findex = lindex - 10;
            }

            if (findex < 0)
            {
                findex = 0;
            }
            string cultureName = SPContext.Current.Web.Locale.Parent.Name;

            //Now creating page number based on above first and last page index
            for (int i = findex; i < lindex; i++)
            {
                DataRow dr = dt.NewRow();

                dr[0] = i;
                dr[1] = (cultureName == "ar") ? ConvertToArabicNumerals(Convert.ToString(i + 1)) : Convert.ToString(i + 1);
                dt.Rows.Add(dr);
            }

            //Finally bind it page numbers in to the Paging DataList "RepeaterPaging"

            RepeaterPaging.DataSource = (dt.Rows.Count > 0) ? dt : null;
            RepeaterPaging.DataBind();
        }
        private int CurrentPage
        {
            get
            {   //Check view state is null if null then return current index value as "0" else return specific page viewstate value
                if (ViewState["CurrentPage"] == null)
                {
                    return 0;
                }
                else
                {
                    return ((int)ViewState["CurrentPage"]);
                }
            }
            set
            {
                //Set View statevalue when page is changed through Paging "RepeaterPaging" DataList
                ViewState["CurrentPage"] = value;
            }
        }
        /// <summary>
        /// Method to get data from pages library 
        /// </summary>
        /// <returns></returns>
        DataTable GetData()
        {
            DataTable eServiceDataTable = null;
            try
            {
                using (SPSite spSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        string rootUrl = spSite.RootWeb.Url;
                        string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                        SPList eServices = spWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}/{2}", rootUrl, sourceVariation, CoreConstants.Lists.News.URL),CoreConstants.Lists.News.URLFromPages));

                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/><FieldRef Name='{1}' Ascending='FALSE'/></OrderBy><Where><BeginsWith><FieldRef Name=\"ContentTypeId\" /><Value Type=\"{3}\">{2}</Value></BeginsWith></Where>", CoreConstants.Lists.News.Fields.NewsDisplayOrder, CoreConstants.Lists.News.Fields.NewsModifiedDate, CoreConstants.Lists.News.PageContentTypeId, CoreConstants.Lists.News.Fields.NewsPageContentType);

                        SPListItemCollection items = eServices.GetItems(query);
                        if (items.Count > 0)
                        {
                            newsGalleryRepeater.Visible = true;
                            eServiceDataTable = items.GetDataTable();
                        }
                        else
                        {
                            newsGalleryRepeater.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                var errorMessage = String.Format("Exception - {0}",ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
            return eServiceDataTable;

        }
        #endregion

        #region Events Paging
        /// <summary>
        /// Repeated Bind Event to bind all news from pages library
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RepeaterPaging_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            //Enabled False for current selected Page index
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("Pagingbtn");
            if (lnkPage.CommandArgument.ToString() == CurrentPage.ToString())
            {
                lnkPage.Enabled = false;
                lnkPage.CssClass = "currentPage";
            }
        }
        /// <summary>
        /// Command argument event for Current Page to implement Paging
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void RepeaterPaging_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName.Equals("newpage"))
            {
                //Assign CurrentPage number when user click on the page number in the Paging "RepeaterPaging" DataList
                CurrentPage = Convert.ToInt32(e.CommandArgument.ToString());
                //Refresh "Repeater1" control Data once user change page
                BindDataList();
            }
        }
        /// <summary>
        /// Previous page button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            //If user click Previous Link button assign current index as -1 it reduce existing page index.
            CurrentPage -= 1;
            //refresh "Repeater1" Data
            BindDataList();
        }
        /// <summary>
        /// Next Page Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkNext_Click(object sender, EventArgs e)
        {
            //If user click Next Link button assign current index as +1 it add one value to existing page index.
            CurrentPage += 1;

            //refresh "Repeater1" Data
            BindDataList();
        }
        #endregion
        #endregion

    }
}
