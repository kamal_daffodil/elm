﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsGalleryUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.NewsGallery.NewsGalleryUserControl" %>

<asp:UpdatePanel runat="server" ID="newsUpdatePanel">
    <ContentTemplate>
        <input type="hidden" runat="server" class="hdnNewsCategory" id="hdnNewsCategory" />
        <div class="categoryPage">
            <asp:Label ID="lblNoNewsFound" runat="server"></asp:Label>
            <div class="categorynewsSection">
                <asp:Repeater runat="server" ID="newsGalleryRepeater" OnItemDataBound="newsGalleryRepeater_OnItemDataBound">
                    <ItemTemplate>
                        <input type="hidden" runat="server" id="hdnNewsCategoryID" value='<%# Eval("LinkFilename")%>' />
                        <div class="categoryNewsBlock">
                            <div class="heading">
                                <a runat="server" id="A1" href='<%# Eval("LinkFilename")%>'>
                                    <asp:Label runat="server" ID="lblNewsTitle" Text='<%# Eval("Title")%>'></asp:Label>
                                </a>
                            </div>
                            <div class="newsImg">
                                <asp:Image ID="imgNews" ImageUrl='<%# GetImageurl(Eval("PublishingRollupImage")) %>'
                                    runat="server" />
                            </div>
                            <p>
                                <%# GetDescriptionText(Eval("ELMDescription"))%>
                                <a runat="server" id="lnkReadMore" href='<%# Eval("LinkFilename")%>'>
                                    <asp:Label runat="server" ID="lblReadMore"></asp:Label>
                                </a>
                            
                        </div>
                    </ItemTemplate>
                    
                </asp:Repeater>
                
                <div class="clr">
                </div>
                <div runat="server" id="divPages" class="pagination">
                    <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><</asp:LinkButton>
                    <asp:DataList ID="RepeaterPaging" runat="server" OnItemCommand="RepeaterPaging_ItemCommand"
                        OnItemDataBound="RepeaterPaging_ItemDataBound" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <ItemTemplate>
                            <asp:LinkButton ID="Pagingbtn" runat="server" CommandArgument='<%# Eval("PageIndex") %>'
                                CommandName="newpage" Text='<%# Eval("PageText") %> '></asp:LinkButton>
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click">></asp:LinkButton>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
