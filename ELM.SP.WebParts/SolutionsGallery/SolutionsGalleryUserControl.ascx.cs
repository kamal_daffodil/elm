﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Collections.Generic;
using Microsoft.SharePoint.Utilities;
using System.Text.RegularExpressions;
using ELM.SP.Library;
namespace ELM.SP.WebParts.SolutionsGallery
{
    public partial class SolutionsGalleryUserControl : UserControl
    {
         
        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SPListItemCollection items = null;
            try
            {
              items = GetData();
              repSolutions.DataSource = items.GetDataTable();
              repSolutions.DataBind();
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
        }
        /// <summary>
        /// Repeater Data Bound Event for solutions on home Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repSolutionAdded_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.DataItem != null)
            {
                ((Label)e.Item.FindControl("lblMore")).Text = Utility.GetLocalizedValue("elm_Service_MoreUrl", locale);
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(object itemData)
        {
            string htmlText = string.Empty;
            if (itemData != null)
            {
                htmlText = Convert.ToString(itemData);
            }

            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// Method to get substring from the data obtained from List Field
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {
            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }

            var descriptionText = string.Empty;

            if (!String.IsNullOrEmpty(itemDataString))
            {
                descriptionText = itemDataString.Length > 200 ? string.Format("{0}...", itemDataString.Substring(0, 180)) : itemDataString;
            }

            return descriptionText;
        }
        /// <summary>
        /// Method to get solutions for home page from lists
        /// </summary>
        /// <returns></returns>
        public SPListItemCollection GetData() 
        {
            SPListItemCollection items = null;
            try
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        string serverUrl = currentWeb.Url;
                        SPList solutions = currentWeb.GetList(SPUrlUtility.CombineUrl(serverUrl, CoreConstants.Lists.Solution.URL));
                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy>", CoreConstants.Lists.Solution.Fields.SolutionsDisplayOrder);
                        query.RowLimit = 3; // we want to retrieve First 3 items
                        items = solutions.GetItems(query);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }
            return items;
        }
        #endregion
    }
}
