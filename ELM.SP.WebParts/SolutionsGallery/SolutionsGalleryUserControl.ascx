﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SolutionsGalleryUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.SolutionsGallery.SolutionsGalleryUserControl" %>
<asp:Repeater runat="server" ID="repSolutions" OnItemDataBound="repSolutionAdded_OnItemDataBound">
    <HeaderTemplate>
        <div class="wrapper">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="topPaneblock">
            <h3>
                <a class="solTitle" href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
                    <%# Eval("ELMPublishingIcon")%>
                    <asp:Label runat="server" ID="lblSolutionsTiltle" Text='<%# Eval("Title") %>'></asp:Label>
                </a>
            </h3>
            <p>
                <asp:Label runat="server" ID="lblSolutionsDescp" Text='<%# ExtractHtmlInnerText(Eval("ELMDescription")) %>'></asp:Label></p>
            <a class="readMore" href=<%# Eval("ELMRedirectURL").ToString().Split(',')[0] %>>
                <asp:Label ID="lblMore" runat="server"></asp:Label>
            </a>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>
