﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="93205430-ebb8-47c3-8de3-18b77d35242e" alwaysForceInstall="true" description="This will deploy all webparts and control templates." featureId="93205430-ebb8-47c3-8de3-18b77d35242e" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.fec200e6-80a1-4242-a6e7-e95d651b0949.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ELM.SP.WebParts Webpart" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="d815a61e-1e3b-45a4-ba6e-98420899dae7" />
    <projectItemReference itemId="df9ae300-0d6e-4b96-9b1d-a37ec8edba5d" />
    <projectItemReference itemId="06b46a46-a39d-40c3-90f5-ad38af7a9a3d" />
    <projectItemReference itemId="071b944a-6ae4-47f7-ba5f-378b84ed6fc3" />
    <projectItemReference itemId="51237839-21b2-4206-8b78-bd1eb82dd4b9" />
    <projectItemReference itemId="3511392d-f4a9-4f54-a8e4-b911bdf8cc7c" />
    <projectItemReference itemId="7622cc61-ebc9-46f0-8a18-86a43e2f42fc" />
    <projectItemReference itemId="f404a153-c1f2-47d0-954a-1d542976232a" />
    <projectItemReference itemId="323c894b-250f-498b-a5ce-768451d2af42" />
    <projectItemReference itemId="9693cc44-d4d2-41e6-8025-4dccf6ca306a" />
    <projectItemReference itemId="9712673c-65cc-49ff-85ee-22cf1bca4fad" />
  </projectItems>
</feature>