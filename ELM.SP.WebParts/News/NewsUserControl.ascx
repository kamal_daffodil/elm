﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.News.NewsUserControl" %>
<asp:Repeater ID="repNews" runat="server" OnItemDataBound="repSolutionAdded_OnItemDataBound">
    <ItemTemplate>
        <div class="newsBlock">
            <input type="hidden" runat="server" id="hdnNewsID" value='<%# ((SPListItem)Container.DataItem)["LinkFilename"] %>' />
            <asp:HyperLink runat="server" class="NewsTitle" ID="lblNewsAndEvents"></asp:HyperLink>
            <asp:Image ID="imgNews" ImageUrl='<%# GetImageurl(((SPListItem)Container.DataItem)["PublishingRollupImage"]) %>'
                runat="server" />
            <h5>
                <a runat="server" id="lnkTitle" href='<%# ((SPListItem)Container.DataItem)["LinkFilename"] %>'>
                    <asp:Label ID="lblTitle" runat="server" Text='<%# ((SPListItem)Container.DataItem)["Title"]%>'></asp:Label>
                </a>
            </h5>
            <p class="newsHomeContent">
                <%# GetDescriptionText(((SPListItem)Container.DataItem)["ELMDescription"])%>
            </p>
            <p>
                <a class="readMore" runat="server" id="anchorForReadMore" href='<%# ((SPListItem)Container.DataItem)["LinkFilename"] %>'>
                    <asp:Label ID="lblMore" runat="server"></asp:Label></a>
            </p>
        </div>
    </ItemTemplate>
</asp:Repeater>
