﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace ELM.SP.WebParts.News
{
    [ToolboxItemAttribute(false)]
    public class News : WebPart
    {
       [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("Redirect link"),
        WebDescription("Please enter the page url to redirect to")]
        public string _redirectUrl { get; set; }


       [Category("Extended Settings"),
        Personalizable(PersonalizationScope.Shared),
        WebBrowsable(true),
        WebDisplayName("News Detail Link"),
        WebDescription("Please enter the page url to redirect to")]
        public string _newsDetailUrl { get; set; }


        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/ELM.SP.WebParts/News/NewsUserControl.ascx";

        protected override void CreateChildControls()
        {
            NewsUserControl NewsControl = (NewsUserControl)Page.LoadControl(_ascxPath);
            if (NewsControl != null)
            {
                NewsControl.oNewsWebpart = this;
                Controls.Add(NewsControl);
            }

        }
    }
}
