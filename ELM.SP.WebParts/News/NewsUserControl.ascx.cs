﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Microsoft.SharePoint.Publishing.Fields;
using ELM.SP.Library;
namespace ELM.SP.WebParts.News
{
    public partial class NewsUserControl : UserControl
    {
         
        #region Properties
        public News oNewsWebpart { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (SPSite currentSite = new SPSite(string.Format("{0}/{1}", SPContext.Current.Web.Url ,CoreConstants.Lists.News.URL)))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        string serverUrl = currentWeb.Url;
                        SPList news = currentWeb.GetList(SPUrlUtility.CombineUrl(serverUrl, CoreConstants.Lists.News.URLFromPages));
                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/><FieldRef Name='{1}' Ascending='FALSE'/></OrderBy><Where><BeginsWith><FieldRef Name=\"ContentTypeId\" /><Value Type=\"{3}\">{2}</Value></BeginsWith></Where>",CoreConstants.Lists.News.Fields.NewsDisplayOrder, CoreConstants.Lists.News.Fields.NewsModifiedDate, CoreConstants.Lists.News.PageContentTypeId,CoreConstants.Lists.News.Fields.NewsPageContentType);
                        query.RowLimit = 1; // we want to retrieve First item
                        SPListItemCollection items = news.GetItems(query);
                        repNews.DataSource = items;
                        repNews.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = String.Format("Exception- {0}", ex);
                LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
            }

        }
        /// <summary>
        /// Repeated Data Bind event to bing News on Home Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repSolutionAdded_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
            if (e.Item.DataItem != null)
            {
                string itemId = Convert.ToString(((HtmlInputHidden)e.Item.FindControl("hdnNewsID")).Value);

                if (string.IsNullOrEmpty(oNewsWebpart._newsDetailUrl))
                {
                    ((HtmlAnchor)e.Item.FindControl("lnkTitle")).HRef = string.Format("/{0}/News/Pages/{1}",sourceVariation,itemId);
                    ((HtmlAnchor)e.Item.FindControl("anchorForReadMore")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                }
                else
                {
                    ((HtmlAnchor)e.Item.FindControl("lnkTitle")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                    ((HtmlAnchor)e.Item.FindControl("anchorForReadMore")).HRef = string.Format("/{0}/News/Pages/{1}", sourceVariation, itemId);
                }
                ((Label)e.Item.FindControl("lblMore")).Text = Utility.GetLocalizedValue("elm_NewsGallery_MoreUrl", locale);
                ((HyperLink)e.Item.FindControl("lblNewsAndEvents")).Text = oNewsWebpart.Title;

                if (string.IsNullOrEmpty(Convert.ToString(oNewsWebpart._redirectUrl)))
                {
                    ((HyperLink)e.Item.FindControl("lblNewsAndEvents")).NavigateUrl = string.Format("/{0}/{1}", SPContext.Current.Web.Locale.Parent.Name, CoreConstants.Lists.News.URL);
                }
                else
                {
                    ((HyperLink)e.Item.FindControl("lblNewsAndEvents")).NavigateUrl = Convert.ToString(oNewsWebpart._redirectUrl);
                }
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="htmlText"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(string htmlText)
        {
            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// Method to get substring from the data obtained from List Field
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {
            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }
           
            var descriptionText = string.Empty;

            if (!String.IsNullOrEmpty(itemDataString))
            {
                descriptionText = itemDataString.Length > 200 ? string.Format("{0}...", itemDataString.Substring(0, 180)) : itemDataString;
            }

            return descriptionText;
        }
        /// <summary>
        /// Method to get image from handler and bind to design
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetImageurl(object itemData)
        {
            string pictureUrl = string.Empty;
            string cultureName = SPContext.Current.Web.Locale.Parent.Name;
            string rootUrl = SPContext.Current.Site.Url;
            try
            {
                Microsoft.SharePoint.Publishing.Fields.ImageFieldValue imageUrl = (Microsoft.SharePoint.Publishing.Fields.ImageFieldValue)itemData;
                string ImageUrl = imageUrl.ImageUrl;

                if (string.IsNullOrEmpty(ImageUrl))
                {
                    pictureUrl = (cultureName == "ar") ? string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsArabicImageURL) : string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsEnglishImageURL);
                }
                else
                {
                    if (ImageUrl.Contains("http://"))
                    {
                        pictureUrl = string.Format("{0}3&imageUrl={1}", CoreConstants.ImageHandlerURL, ImageUrl);
                    }
                    else
                    {
                        pictureUrl = string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, ImageUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerService.LogError(LoggerService.LOG_ERROR, string.Format("Exception Caught - {0}", ex.Message));
                pictureUrl = (cultureName == "ar") ? string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsArabicImageURL) : string.Format("{0}3&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, CoreConstants.Lists.News.DefaultNewsEnglishImageURL);

            }
            return pictureUrl;
        }
        #endregion
    }
}
