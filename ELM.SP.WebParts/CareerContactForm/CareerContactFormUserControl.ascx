﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerContactFormUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.CareerContactForm.CareerContactFormUserControl" %>
<div class="mainContent">
    <div class="wrapper containsBackground">
    <div class="sub_title">
        <asp:Label ID="lblContactText" runat="server"></asp:Label><div class="star">
            <span class="DevHelperRequiredMark">*</span><span id="spanRequiredText" runat="server"></span>
        </div>
    </div>
    <!-- Messages Panel -->
    <table width="100%">
        <tbody>
            <tr>
                <td>
                    <!-- Error -->

                    <asp:HiddenField ID="hdnViewID" runat="server" Value="0" />
                   
                    <!-- Success -->

                    <asp:Panel ID="pnlSuccessDiv" runat="server" Visible="false">
                        <div id="divSuccess">
                            <table width="100%" cellspacing="0" cellpadding="2" border="0" class="message">
                                <tbody>
                                    <tr>
                                        <td class="message_icon">
                                        </td>
                                        <td>
                                            <span id="spanCompletion" runat="server"></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </asp:Panel>
                    <!-- Success Panel Ends-->
                </td>
            </tr>
        </tbody>
    </table>
    <div style="width: 100%;">
        <asp:Panel ID="pnlContact" runat="server">
            <table width="100%">
                <tbody>
                    <tr>
                        <td class="formLabelTd">
                        <asp:HiddenField ID="hdnCaptchaKey" runat="server" />
                            <span runat="server" id="spanTitle"></span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox runat="server" ID="textTitle" CssClass="formTextBox"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblName"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox ID="textName" runat="server" CssClass="formTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorName" runat="server" ControlToValidate="textName" Display="Dynamic"
                                ValidationGroup="SendDetailsGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblCountry"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:DropDownList ID="selctCountry" runat="server" CssClass="formDropDown">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="validatorCountry" runat="server" Display="Dynamic"
                                 ValidationGroup="SendDetailsGroup" ControlToValidate="selctCountry"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblCity"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox ID="textCity" runat="server" CssClass="formTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorCity" runat="server" ControlToValidate="textCity" Display="Dynamic"
                                ValidationGroup="SendDetailsGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblEmail"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox ID="textEmail" runat="server" CssClass="formTextBox" />
                            <asp:RequiredFieldValidator ID="validatorEmailRequired" runat="server" ControlToValidate="textEmail" Display="Dynamic"
                                ValidationGroup="SendDetailsGroup"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="validatorEmail" ControlToValidate="textEmail" Display="Dynamic"
                                runat="server"  ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ValidationGroup="SendDetailsGroup"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblMobile"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox runat="server" ID="textMobile" CssClass="formTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorMobile" runat="server" ControlToValidate="textMobile" Display="Dynamic"
                                 ValidationGroup="SendDetailsGroup"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <label runat="server" id="lblMessage"></label><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <asp:TextBox TextMode="MultiLine" ID="textMessage" runat="server" CssClass="formMultiTextBox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="validatorMessage" runat="server" Display="Dynamic"
                                ValidationGroup="SendDetailsGroup" ControlToValidate="textMessage"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="formLabelTd">
                            <span runat="server" id="spanCode"></span><span class="DevHelperRequiredMark">*</span>
                        </td>
                        <td class="formControlTd">
                            <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
                                <tbody>
                                    <tr>
                                        <td>
                                        <asp:Image ID="imgCaptcha" runat="server" />   
                                            <br />                                            
                                            <asp:TextBox ID="txtCaptchaCode" runat="server" MaxLength="6" CssClass="formTextBox" />
                                            <asp:RequiredFieldValidator
                                                ID="validatorCaptcha" runat="server" Display="Dynamic"
                                                ValidationGroup="SendDetailsGroup" ControlToValidate="txtCaptchaCode"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lblWrongCode" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" class="blue-button">
                            <asp:Button ID="btnSend" runat="server" Text="Send" ValidationGroup="SendDetailsGroup"
                                OnClick="BtnSend_Click" CssClass="form_btn" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel"
                                OnClick="BtnCancel_Click" CssClass="form_btn" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </div>
</div>
</div>
