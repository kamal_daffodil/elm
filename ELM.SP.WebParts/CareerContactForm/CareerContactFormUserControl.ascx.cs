﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using ELM.SP.Library;
using System.Text;
using System.Security.Cryptography;
using System.IO;
namespace ELM.SP.WebParts.CareerContactForm
{
    public partial class CareerContactFormUserControl : UserControl
    {
         

        #region Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;

            if (Microsoft.SharePoint.SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
            {
                pnlSuccessDiv.Visible = false;
                pnlContact.Visible = false;

            }

            else
            {
                if (hdnViewID.Value == "0")
                {
                    pnlSuccessDiv.Visible = false;
                    pnlContact.Visible = true;
                }
                else
                {
                    pnlSuccessDiv.Visible = true;
                }

                if (!IsPostBack)
                {
                    hdnCaptchaKey.Value = Utility.Encrypt(Utility.GetRandomCaptchaValue());
                    imgCaptcha.ImageUrl = string.Format("{0}{1}", CoreConstants.CaptchaHandlerURL, hdnCaptchaKey.Value);

                    //Setting label Messages
                    SetLabelText(locale);
                    //Setting Validator Messages
                    SetValidationMessages(locale);
                    //Items for DropDown
                    FillDropDown(locale);
                }
            }
        }
        /// <summary>
        /// Send Button Event to fill in contact form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnSend_Click(Object sender, EventArgs e)
        {
            lblWrongCode.Visible = false;
             
            uint localeCurrent = (uint)SPContext.Current.Web.Locale.LCID;
            string hdnCaptchaKeyText = Utility.Decrypt(hdnCaptchaKey.Value);
            if (hdnCaptchaKeyText == txtCaptchaCode.Text)
            {
                try
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
              {
                  using (SPSite currentSite = new SPSite(SPContext.Current.Web.Url))
                  {
                      using (SPWeb currentWeb = currentSite.OpenWeb())
                      {
                          if (hdnViewID.Value == "0")
                          {
                              string rootUrl = currentSite.RootWeb.Url;
                              string sourceVariation = SPContext.Current.Web.Locale.Parent.Name;
                              SPList careerContactRecordList = currentWeb.GetList(SPUrlUtility.CombineUrl(string.Format("{0}/{1}", rootUrl, sourceVariation), CoreConstants.Lists.CareerContactForm.URL));
                              SPListItemCollection listItems = careerContactRecordList.Items;
                              SPListItem item = listItems.Add();

                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactTitle] = textTitle.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactName] = textName.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactCountry] = selctCountry.SelectedItem.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactCity] = textCity.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactEmail] = textEmail.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactMobile] = textMobile.Text;
                              item[CoreConstants.Lists.CareerContactForm.Fields.CareerContactMessage] = textMessage.Text;
                              currentWeb.AllowUnsafeUpdates = true;
                              item.Update();
                              currentWeb.AllowUnsafeUpdates = false;
                          }
                      }
                  }
              });
                }
                catch (Exception exception)
                {
                    var errorMessage = string.Format("Exception Caught - {0}", exception.Message);
                    LoggerService.LogError(LoggerService.LOG_ERROR, errorMessage);
                }
                pnlSuccessDiv.Visible = true;
                ClearFields();
            }
            else
            {
                lblWrongCode.Visible = true;
                validatorCaptcha.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Wrong_CaptchaCode", localeCurrent);
            }
        }
        /// <summary>
        /// Cancel Button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnCancel_Click(Object sender, EventArgs e)
        {
            ClearFields();
        }
        #endregion

        #region Private Members
        /// <summary>
        /// Method to clear all fields
        /// </summary>
        private void ClearFields()
        {
            textTitle.Text = string.Empty;
            textName.Text = string.Empty;
            selctCountry.SelectedIndex = -1;
            textCity.Text = string.Empty;
            textEmail.Text = string.Empty;
            textMobile.Text = string.Empty;
            textMessage.Text = string.Empty;
            txtCaptchaCode.Text = string.Empty;
        }
        /// <summary>
        /// Method to set text over labels
        /// </summary>
        /// <param name="locale"></param>
        private void SetLabelText(uint locale)
        {
            #region Label Inner text From resource file
            spanTitle.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Title", locale);
            lblName.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Name", locale);
            lblCountry.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Country", locale);
            lblCity.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_City", locale);
            lblEmail.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Email", locale);
            lblMessage.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Message", locale);
            lblMobile.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_Mobile", locale);
            spanCode.InnerText = Utility.GetLocalizedValue("elm_Carrer_Contact_CaptchaCode", locale);
            btnSend.Text = Utility.GetLocalizedValue("elm_Career_Contact_SendBtn", locale);
            btnCancel.Text = Utility.GetLocalizedValue("elm_Career_Contact_CancelBtn", locale);
            spanCompletion.InnerText = Utility.GetLocalizedValue("elm_Career_Contact_Completion", locale);
            spanRequiredText.InnerText = Utility.GetLocalizedValue("elm_Career_Contact_RequiredText", locale);
            validatorCountry.InitialValue = Utility.GetLocalizedValue("elm_Career_Contact_Select_Common", locale);
            lblContactText.Text = Utility.GetLocalizedValue("elm_Common_ContactText", locale);
            lblWrongCode.Text = Utility.GetLocalizedValue("elm_Career_Contact_WrongCode", locale);
            #endregion
        }
        /// <summary>
        /// Method to Validator messages
        /// </summary>
        /// <param name="locale"></param>
        private void SetValidationMessages(uint locale)
        {
            #region Validator error messages from resource file
            validatorEmail.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Email", locale);
            validatorEmailRequired.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_EmailRequired", locale);
            validatorMessage.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Message", locale);
            validatorMobile.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Mobile", locale);
            validatorName.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Name", locale);
            validatorCountry.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_Country", locale);
            validatorCaptcha.ErrorMessage = Utility.GetLocalizedValue("elm_Carrer_Contact_Validator_CaptchaCode", locale);
            #endregion
        }
        /// <summary>
        /// Method to fill dropdown values
        /// </summary>
        /// <param name="locale"></param>
        private void FillDropDown(uint locale)
        {
            #region DropDown values for country from Resource Files
            selctCountry.Items.Insert(0, Utility.GetLocalizedValue("elm_Career_Contact_Select_Common", locale));
            selctCountry.Items.Insert(1, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_1", locale));
            selctCountry.Items.Insert(2, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_2", locale));
            selctCountry.Items.Insert(3, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_3", locale));
            selctCountry.Items.Insert(4, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_4", locale));
            selctCountry.Items.Insert(5, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_5", locale));
            selctCountry.Items.Insert(6, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_6", locale));
            selctCountry.Items.Insert(7, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_7", locale));
            selctCountry.Items.Insert(8, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_8", locale));
            selctCountry.Items.Insert(9, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_9", locale));
            selctCountry.Items.Insert(10, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_10", locale));
            selctCountry.Items.Insert(11, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_11", locale));
            selctCountry.Items.Insert(12, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_12", locale));
            selctCountry.Items.Insert(13, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_13", locale));
            selctCountry.Items.Insert(14, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_14", locale));
            selctCountry.Items.Insert(15, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_15", locale));
            selctCountry.Items.Insert(16, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_16", locale));
            selctCountry.Items.Insert(17, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_17", locale));
            selctCountry.Items.Insert(18, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_18", locale));
            selctCountry.Items.Insert(19, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_19", locale));
            selctCountry.Items.Insert(20, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_20", locale));
            selctCountry.Items.Insert(21, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_21", locale));
            selctCountry.Items.Insert(22, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_22", locale));
            selctCountry.Items.Insert(23, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_23", locale));
            selctCountry.Items.Insert(24, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_24", locale));
            selctCountry.Items.Insert(25, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_25", locale));
            selctCountry.Items.Insert(26, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_26", locale));
            selctCountry.Items.Insert(27, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_27", locale));
            selctCountry.Items.Insert(28, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_28", locale));
            selctCountry.Items.Insert(29, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_29", locale));
            selctCountry.Items.Insert(30, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_30", locale));
            selctCountry.Items.Insert(31, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_31", locale));
            selctCountry.Items.Insert(22, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_32", locale));
            selctCountry.Items.Insert(33, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_33", locale));
            selctCountry.Items.Insert(34, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_34", locale));
            selctCountry.Items.Insert(35, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_35", locale));
            selctCountry.Items.Insert(36, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_36", locale));
            selctCountry.Items.Insert(37, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_37", locale));
            selctCountry.Items.Insert(38, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_38", locale));
            selctCountry.Items.Insert(39, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_39", locale));
            selctCountry.Items.Insert(40, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_40", locale));
            selctCountry.Items.Insert(41, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_41", locale));
            selctCountry.Items.Insert(42, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_42", locale));
            selctCountry.Items.Insert(43, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_43", locale));
            selctCountry.Items.Insert(44, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_44", locale));
            selctCountry.Items.Insert(45, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_45", locale));
            selctCountry.Items.Insert(46, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_46", locale));
            selctCountry.Items.Insert(47, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_47", locale));
            selctCountry.Items.Insert(48, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_48", locale));
            selctCountry.Items.Insert(49, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_49", locale));
            selctCountry.Items.Insert(50, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_50", locale));
            selctCountry.Items.Insert(51, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_51", locale));
            selctCountry.Items.Insert(52, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_52", locale));
            selctCountry.Items.Insert(53, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_53", locale));
            selctCountry.Items.Insert(54, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_54", locale));
            selctCountry.Items.Insert(55, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_55", locale));
            selctCountry.Items.Insert(56, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_56", locale));
            selctCountry.Items.Insert(57, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_57", locale));
            selctCountry.Items.Insert(58, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_58", locale));
            selctCountry.Items.Insert(59, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_59", locale));
            selctCountry.Items.Insert(60, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_60", locale));
            selctCountry.Items.Insert(61, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_61", locale));
            selctCountry.Items.Insert(62, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_62", locale));
            selctCountry.Items.Insert(63, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_63", locale));
            selctCountry.Items.Insert(64, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_64", locale));
            selctCountry.Items.Insert(65, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_65", locale));
            selctCountry.Items.Insert(66, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_66", locale));
            selctCountry.Items.Insert(67, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_67", locale));
            selctCountry.Items.Insert(68, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_68", locale));
            selctCountry.Items.Insert(69, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_69", locale));
            selctCountry.Items.Insert(70, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_70", locale));
            selctCountry.Items.Insert(71, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_71", locale));
            selctCountry.Items.Insert(72, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_72", locale));
            selctCountry.Items.Insert(73, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_73", locale));
            selctCountry.Items.Insert(74, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_74", locale));
            selctCountry.Items.Insert(75, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_75", locale));
            selctCountry.Items.Insert(76, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_76", locale));
            selctCountry.Items.Insert(77, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_77", locale));
            selctCountry.Items.Insert(78, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_78", locale));
            selctCountry.Items.Insert(79, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_79", locale));
            selctCountry.Items.Insert(80, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_80", locale));
            selctCountry.Items.Insert(81, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_81", locale));
            selctCountry.Items.Insert(82, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_82", locale));
            selctCountry.Items.Insert(83, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_83", locale));
            selctCountry.Items.Insert(84, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_84", locale));
            selctCountry.Items.Insert(85, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_85", locale));
            selctCountry.Items.Insert(86, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_86", locale));
            selctCountry.Items.Insert(87, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_87", locale));
            selctCountry.Items.Insert(88, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_88", locale));
            selctCountry.Items.Insert(89, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_89", locale));
            selctCountry.Items.Insert(90, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_90", locale));
            selctCountry.Items.Insert(91, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_91", locale));
            selctCountry.Items.Insert(92, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_92", locale));
            selctCountry.Items.Insert(93, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_93", locale));
            selctCountry.Items.Insert(94, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_94", locale));
            selctCountry.Items.Insert(95, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_95", locale));
            selctCountry.Items.Insert(96, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_96", locale));
            selctCountry.Items.Insert(97, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_97", locale));
            selctCountry.Items.Insert(98, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_98", locale));
            selctCountry.Items.Insert(99, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_99", locale));
            selctCountry.Items.Insert(100, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_100", locale));
            selctCountry.Items.Insert(101, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_101", locale));
            selctCountry.Items.Insert(102, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_102", locale));
            selctCountry.Items.Insert(103, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_103", locale));
            selctCountry.Items.Insert(104, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_104", locale));
            selctCountry.Items.Insert(105, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_105", locale));
            selctCountry.Items.Insert(106, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_106", locale));
            selctCountry.Items.Insert(107, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_107", locale));
            selctCountry.Items.Insert(108, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_108", locale));
            selctCountry.Items.Insert(109, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_109", locale));
            selctCountry.Items.Insert(110, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_110", locale));
            selctCountry.Items.Insert(111, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_111", locale));
            selctCountry.Items.Insert(112, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_112", locale));
            selctCountry.Items.Insert(113, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_113", locale));
            selctCountry.Items.Insert(114, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_114", locale));
            selctCountry.Items.Insert(115, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_115", locale));
            selctCountry.Items.Insert(116, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_116", locale));
            selctCountry.Items.Insert(117, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_117", locale));
            selctCountry.Items.Insert(118, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_118", locale));
            selctCountry.Items.Insert(119, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_119", locale));
            selctCountry.Items.Insert(120, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_120", locale));
            selctCountry.Items.Insert(121, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_121", locale));
            selctCountry.Items.Insert(122, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_122", locale));
            selctCountry.Items.Insert(123, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_123", locale));
            selctCountry.Items.Insert(124, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_124", locale));
            selctCountry.Items.Insert(125, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_125", locale));
            selctCountry.Items.Insert(126, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_126", locale));
            selctCountry.Items.Insert(127, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_127", locale));
            selctCountry.Items.Insert(128, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_128", locale));
            selctCountry.Items.Insert(129, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_129", locale));
            selctCountry.Items.Insert(130, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_130", locale));
            selctCountry.Items.Insert(131, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_131", locale));
            selctCountry.Items.Insert(122, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_132", locale));
            selctCountry.Items.Insert(133, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_133", locale));
            selctCountry.Items.Insert(134, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_134", locale));
            selctCountry.Items.Insert(135, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_135", locale));
            selctCountry.Items.Insert(136, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_136", locale));
            selctCountry.Items.Insert(137, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_137", locale));
            selctCountry.Items.Insert(138, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_138", locale));
            selctCountry.Items.Insert(139, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_139", locale));
            selctCountry.Items.Insert(140, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_140", locale));
            selctCountry.Items.Insert(141, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_141", locale));
            selctCountry.Items.Insert(142, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_142", locale));
            selctCountry.Items.Insert(143, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_143", locale));
            selctCountry.Items.Insert(144, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_144", locale));
            selctCountry.Items.Insert(145, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_145", locale));
            selctCountry.Items.Insert(146, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_146", locale));
            selctCountry.Items.Insert(147, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_147", locale));
            selctCountry.Items.Insert(148, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_148", locale));
            selctCountry.Items.Insert(149, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_149", locale));
            selctCountry.Items.Insert(150, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_150", locale));
            selctCountry.Items.Insert(151, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_151", locale));
            selctCountry.Items.Insert(152, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_152", locale));
            selctCountry.Items.Insert(153, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_153", locale));
            selctCountry.Items.Insert(154, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_154", locale));
            selctCountry.Items.Insert(155, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_155", locale));
            selctCountry.Items.Insert(156, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_156", locale));
            selctCountry.Items.Insert(157, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_157", locale));
            selctCountry.Items.Insert(158, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_158", locale));
            selctCountry.Items.Insert(159, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_159", locale));
            selctCountry.Items.Insert(160, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_160", locale));
            selctCountry.Items.Insert(161, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_161", locale));
            selctCountry.Items.Insert(162, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_162", locale));
            selctCountry.Items.Insert(163, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_163", locale));
            selctCountry.Items.Insert(164, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_164", locale));
            selctCountry.Items.Insert(165, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_165", locale));
            selctCountry.Items.Insert(166, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_166", locale));
            selctCountry.Items.Insert(167, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_167", locale));
            selctCountry.Items.Insert(168, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_168", locale));
            selctCountry.Items.Insert(169, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_169", locale));
            selctCountry.Items.Insert(170, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_170", locale));
            selctCountry.Items.Insert(171, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_171", locale));
            selctCountry.Items.Insert(172, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_172", locale));
            selctCountry.Items.Insert(173, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_173", locale));
            selctCountry.Items.Insert(174, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_174", locale));
            selctCountry.Items.Insert(175, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_175", locale));
            selctCountry.Items.Insert(176, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_176", locale));
            selctCountry.Items.Insert(177, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_177", locale));
            selctCountry.Items.Insert(178, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_178", locale));
            selctCountry.Items.Insert(179, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_179", locale));
            selctCountry.Items.Insert(180, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_180", locale));
            selctCountry.Items.Insert(181, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_181", locale));
            selctCountry.Items.Insert(182, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_182", locale));
            selctCountry.Items.Insert(183, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_183", locale));
            selctCountry.Items.Insert(184, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_184", locale));
            selctCountry.Items.Insert(185, Utility.GetLocalizedValue("elm_Career_Contact_Select_Country_185", locale));
            #endregion
        }
        #endregion
    }
}
