﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages"
    Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="eServicesUserControl.ascx.cs"
    Inherits="ELM.SP.WebParts.eServices.eServicesUserControl" %>

<link href="/_layouts/ELM/eServices/owl.carousel.css" rel="stylesheet" />
<link href="/_layouts/ELM/eServices/owl.theme.css" rel="stylesheet" />
<link href="/_layouts/ELM/eServices/component.css" rel="stylesheet" />
<link href="/_layouts/ELM/eServices/default.css" rel="stylesheet" />
<script src="/_layouts/ELM/eServices/modernizr.custom.js" type="text/javascript" ></script>
<script src="/_layouts/ELM/eServices/toucheffects.js" type="text/javascript" ></script>

<input type="hidden" runat="server" id="hdnLabel" class="hdnLabel" />

<asp:Repeater runat="server" ID="eServicesRepeater" OnItemDataBound="ServiceOnItemDataBound">
    <HeaderTemplate>
    <div class="horizontal-ruler"></div>
        <div id="owl-demo">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="container demo-3">
        <div class="item grid cs-style-3">
		<figure>
            <a id="A1" runat="server" href=<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(",".ToCharArray())[0] %> class="serviceImg">
                <asp:Image ID="imgServices" ImageUrl='<%# GetImageurl(((SPListItem)Container.DataItem)["ELMPublishingImage"]) %>' runat="server" />

            </a>
           <figcaption>
           <a id="A2" runat="server" href=<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(",".ToCharArray())[0] %>>
            <h4 style="color:#fff;">
            <asp:Label ID="lblMore" runat="server"></asp:Label>
            </h4>
           </a>
            <p class="testP">
                <%# GetDescriptionText(((SPListItem)Container.DataItem)["ELMDescription"])%>
            </p>
        </figcaption>
			</figure>
             <a href='<%# ((SPListItem)Container.DataItem)["ELMRedirectURL"].ToString().Split(',')[0] %>'>
            <h4 style="color:<%# ((SPListItem)Container.DataItem)["ELMTitleColorCode"] %>" class="testh4">
                <%# ((SPListItem)Container.DataItem)["Title"]%>
            </h4>
            </a>
        </div></div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>


<script src="/_layouts/ELM/eServices/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            items: 3,
            navigation: true,
            pagination: false,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            rewindNav: false
        });

        var LabelText = $('.hdnLabel').val();
        $('#lblProductHeading').html(LabelText);

    });
    </script>
