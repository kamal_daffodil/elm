﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using System.Data;
using Microsoft.SharePoint.Utilities;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using ELM.SP.Library;
namespace ELM.SP.WebParts.eServices
{
    public partial class eServicesUserControl : UserControl
    {
         

        #region Properties
        public eServices oServiceWebpart { get; set; }
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnLabel.Value = oServiceWebpart.Title;
            GetData();
        }
        /// <summary>
        /// Repeater binding event to bind services in carousel 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ServiceOnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            uint locale = (uint)SPContext.Current.Web.Locale.LCID;
            if (e.Item.DataItem != null)
            {
                ((Label)e.Item.FindControl("lblMore")).Text = Utility.GetLocalizedValue("elm_Service_MoreUrl", locale);
            }
        }
        #endregion

        #region Private Members
        /// <summary>
        /// Method to get services from SubService list
        /// </summary>
        private void GetData() 
        {
            try
            {
                using (SPSite spSite = new SPSite(SPContext.Current.Web.Url))
                {
                    using (SPWeb spWeb = spSite.OpenWeb())
                    {
                        string serverUrl = spWeb.Url;
                        SPList eServices = spWeb.GetList(SPUrlUtility.CombineUrl(serverUrl, CoreConstants.Lists.SubService.URL));
                        SPQuery query = new SPQuery();
                        query.Query = string.Format("<OrderBy><FieldRef Name='{0}' Ascending='TRUE'/></OrderBy>", CoreConstants.Lists.SubService.Fields.ServiceDisplayOrder);
                        SPListItemCollection items = eServices.GetItems(query);
                        DataTable eServiceDataTable = items.GetDataTable();
                        eServicesRepeater.DataSource = items;
                        eServicesRepeater.DataBind();
                    }
                }
            }
            catch (Exception ex) 
            {
                LoggerService.LogError(LoggerService.LOG_ERROR, string.Format("Exception Caught - {0}", ex.Message));
            }
        }
        #endregion

        #region Public Members
        /// <summary>
        /// Method to get image from handler and bind to design
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetImageurl(object itemData)
        {
            string rootUrl = SPContext.Current.Site.Url;
            Microsoft.SharePoint.Publishing.Fields.ImageFieldValue imageUrl = (Microsoft.SharePoint.Publishing.Fields.ImageFieldValue)itemData;
            string ImageUrl = imageUrl.ImageUrl;
            string pictureUrl = string.Empty;
            if (ImageUrl.Contains("http://"))
            {
                pictureUrl = string.Format("{0}2&imageUrl={1}", CoreConstants.ImageHandlerURL, ImageUrl);
            }
            else 
            {
                pictureUrl = string.Format("{0}2&imageUrl={1}{2}", CoreConstants.ImageHandlerURL, rootUrl, ImageUrl);
            }
           
            return pictureUrl;
        }
        /// <summary>
        /// Method to get plain text from html content
        /// </summary>
        /// <param name="htmlText"></param>
        /// <returns></returns>
        public static string ExtractHtmlInnerText(string htmlText)
        {
            //Match any Html tag (opening or closing tags) 
            // followed by any successive whitespaces
            //consider the Html text as a single line

            Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

            // replace all html tags (and consequtive whitespaces) by spaces
            // trim the first and last space

            string resultText = regex.Replace(htmlText, " ").Trim();

            return resultText;
        }
        /// <summary>
        /// method to get text to be dispalyed in webpart
        /// </summary>
        /// <param name="itemData"></param>
        /// <returns></returns>
        public string GetDescriptionText(object itemData)
        {

            var itemDataString = string.Empty;
            if (itemData != null)
            {
                itemDataString = ExtractHtmlInnerText(Convert.ToString(itemData));
            }

            var descriptionText = string.Empty;

            if (!String.IsNullOrEmpty(itemDataString))
            {

                descriptionText = itemDataString.Length > 120 ? string.Format("{0}...", itemDataString.Substring(0, 120)) : itemDataString;


            }

            return descriptionText;
        }
        #endregion
    }
}
