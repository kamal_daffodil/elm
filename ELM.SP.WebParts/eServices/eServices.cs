﻿using System;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;

namespace ELM.SP.WebParts.eServices
{
    [ToolboxItemAttribute(false)]
    public class eServices : WebPart
    {
        // Visual Studio might automatically update this path when you change the Visual Web Part project item.
        private const string _ascxPath = @"~/_CONTROLTEMPLATES/ELM.SP.WebParts/eServices/eServicesUserControl.ascx";

        protected override void CreateChildControls()
        {
            eServicesUserControl eServicesControl = (eServicesUserControl)Page.LoadControl(_ascxPath);
            if (eServicesControl != null) 
            {
                eServicesControl.oServiceWebpart = this;
                Controls.Add(eServicesControl);
            }
                        
        }
    }
}
