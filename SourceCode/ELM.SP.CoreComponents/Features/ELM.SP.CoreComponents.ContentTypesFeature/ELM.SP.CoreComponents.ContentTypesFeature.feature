﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="64741e47-6af6-4451-bae5-b6a5238fa346" alwaysForceInstall="true" description="This will deploy all content types at Site Collection Level." featureId="64741e47-6af6-4451-bae5-b6a5238fa346" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ELM.SP.CoreComponents Content Types" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="8777da02-deb8-4d40-8cec-6ea4ebe0ff1a" />
    <projectItemReference itemId="704058ee-65ab-4f0d-87d0-394954bdb229" />
    <projectItemReference itemId="c9099f26-86b3-4aef-a8ab-17fa0ac147c7" />
  </projectItems>
</feature>