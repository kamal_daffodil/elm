﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="1471a74e-30a8-4990-bc28-f8bfd407685c" alwaysForceInstall="true" description="This will deploy all list definition and list instance at Web." featureId="1471a74e-30a8-4990-bc28-f8bfd407685c" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="ELM.SP.CoreComponents Lists" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="fb706594-20cb-4ed1-b71a-cdfd628160c4" />
    <projectItemReference itemId="5d7e62a9-c237-440b-9635-f01852bf5421" />
    <projectItemReference itemId="6c68bda7-603a-458a-94a2-22230ba21823" />
    <projectItemReference itemId="eeb87e6c-490d-483e-afaf-9a24a23d001f" />
    <projectItemReference itemId="01609745-ff54-44f2-8463-be40a9b37658" />
    <projectItemReference itemId="fced8b70-b4a5-42fb-b2cf-9965d7161e68" />
    <projectItemReference itemId="c5ee85bb-6af6-4c02-bb61-6a8a99c2d5e4" />
    <projectItemReference itemId="b4cfe91e-5295-4d3e-a174-b002990e2a3f" />
    <projectItemReference itemId="2b79da11-1945-4e34-bf33-dbf40c980584" />
    <projectItemReference itemId="fdda1383-74e5-40db-9f21-530668842882" />
    <projectItemReference itemId="ec90b712-206c-4ee0-8458-f6312c842d08" />
    <projectItemReference itemId="c6cb04f1-7dd7-47ac-ac4e-97b8c1881342" />
    <projectItemReference itemId="316cbbe2-0fd6-477d-8ee3-4305c85a4732" />
    <projectItemReference itemId="ac513b2b-53b2-4e2d-b90a-42b3948cceb6" />
    <projectItemReference itemId="3f51235a-02d2-4d82-ac3c-7e578ba45ea7" />
    <projectItemReference itemId="1cc78f3c-98b0-4c3f-a757-09c56203a1ec" />
    <projectItemReference itemId="6bf78e64-ccb8-40b4-8ab8-d277ec3a7449" />
    <projectItemReference itemId="107fcac2-2874-459a-9884-6217e959acd6" />
  </projectItems>
</feature>