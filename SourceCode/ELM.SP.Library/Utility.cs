﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Microsoft.SharePoint.Utilities;
using System.IO;
namespace ELM.SP.Library
{
    public static class Utility
    {
        #region Constants used for encryption and decryption process
        private const string passPhrase = "Pas5pr@se";
        // can be any string
        private const string saltValue = "s@1tValue";
        // can be any string
        private const string hashAlgorithm = "SHA1";             // can be "MD5"
        private const int passwordIterations = 2;
        // can be any number
        private const string initVector = "@1B2c3D4e5F6g7H8";
        private const int keySize = 128;
        #endregion

        #region Encryption/Decryption Methods
        /// <summary>
        /// get random string to generate captcha key
        /// </summary>
        /// <returns></returns>
        public static string GetRandomCaptchaValue()
        {
            Random oRandom = new Random();
            int iNumber = oRandom.Next(100000, 999999);
            return iNumber.ToString();
        }
        /// <summary>
        /// Method to encrypt captcha code to be viewable on UI
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public static string Encrypt(string plainText)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            var password = new PasswordDeriveBytes(
                passPhrase,
                saltValueBytes,
                hashAlgorithm,
                passwordIterations);

            byte[] keyBytes = password.GetBytes(keySize / 8);

            var symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                keyBytes,
                initVectorBytes);

            var memoryStream = new MemoryStream();

            var cryptoStream = new CryptoStream(memoryStream,
                                                encryptor,
                                                CryptoStreamMode.Write);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

            cryptoStream.FlushFinalBlock();

            byte[] cipherTextBytes = memoryStream.ToArray();

            memoryStream.Close();
            cryptoStream.Close();

            string cipherText = Convert.ToBase64String(cipherTextBytes);

            return cipherText;
        }
        /// <summary>
        /// Method to decrypt captcha code value to match with value entered in UI
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        public static string Decrypt(string cipherText)
        {
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            var password = new PasswordDeriveBytes(
                passPhrase,
                saltValueBytes,
                hashAlgorithm,
                passwordIterations);

            byte[] keyBytes = password.GetBytes(keySize / 8);

            var symmetricKey = new RijndaelManaged();

            symmetricKey.Mode = CipherMode.CBC;

            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                keyBytes,
                initVectorBytes);

            var memoryStream = new MemoryStream(cipherTextBytes);

            var cryptoStream = new CryptoStream(memoryStream,
                                                decryptor,
                                                CryptoStreamMode.Read);

            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                       0,
                                                       plainTextBytes.Length);

            memoryStream.Close();
            cryptoStream.Close();

            string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                       0,
                                                       decryptedByteCount);

            return plainText;
        }
        #endregion
        /// <summary>
        /// Method to get value of inputKey from resource file according to Variation
        /// </summary>
        /// <param name="inputKey"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        public static string GetLocalizedValue(string inputKey, uint locale)
        {
            string returnValue = string.Empty;

            returnValue = SPUtility.GetLocalizedString("$Resources: " + inputKey, "elm_WebParts", locale);

            return returnValue;
        }
    }
}
