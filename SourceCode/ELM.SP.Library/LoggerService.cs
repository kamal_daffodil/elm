﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;

namespace ELM.SP.Library
{
    public class LoggerService : SPDiagnosticsServiceBase
    {

        #region  Constants

        /// <summary>
        /// public constant variable holding the Info message prefix.
        /// </summary>
        public const string LOG_INFO = "ELM_INFO";

        /// <summary>
        /// public constant variable holding the warning message prefix.
        /// </summary>
        public const string LOG_WARNING = "ELM_WARNING";

        /// <summary>
        /// public constant variable holding the Error message prefix.
        /// </summary>
        public const string LOG_ERROR = "ELM_ERROR";

        /// <summary>
        /// public constant variable holding the Error message prefix.
        /// </summary>
        public const string LOG_ERRORCRITICAL = "ELM_UNEXPECTED";

        /// <summary>
        /// public constant variable holding the Error message prefix.
        /// </summary>
        public const string LOG_MEDIUM = "ELM_MEDIUM";


        /// <summary>
        /// public constant variable holding the product diagnostic name.
        /// </summary>
        private const string PRODUCT_DIAGNOSTIC_NAME = "ELMPublicSite";

        /// <summary>
        /// public constant variable holding the categoryname as string.
        /// </summary>
        private const string SharePointDevelopment = "SharePointDevelopment";


        #endregion

        #region Private Constructor

        /// <summary>
        /// Private constructor for the class to make it Singleton.
        /// </summary>
        private LoggerService() : base("ELMExceptionService", SPFarm.Local) { }

        #endregion

        #region   Singleton Property

        /// <summary>
        /// private static variable.
        /// </summary>
        private static LoggerService _getLogger;

        /// <summary>
        /// private static property to get the object of logger class.
        /// </summary>
        public static LoggerService GetLogger
        {
            get
            {
                if (_getLogger == null)
                    _getLogger = new LoggerService();
                return _getLogger;
            }
        }


        #endregion

        #region  Methods

        /// <summary>
        /// The Method is provided by SPDiagnosticsServiceBase interface.Method defines in which type of logs(INFO,ERROR,EXCEPTION) are to be written in 
        /// SP logs.
        /// </summary>
        /// <returns>An IEnumerable Type of argument.</returns>
        protected override IEnumerable<SPDiagnosticsArea> ProvideAreas()
        {
            List<SPDiagnosticsArea> areas = new List<SPDiagnosticsArea>
            {
                new SPDiagnosticsArea(PRODUCT_DIAGNOSTIC_NAME,new List<SPDiagnosticsCategory>
                {
                    new SPDiagnosticsCategory(LOG_INFO,TraceSeverity.Verbose,EventSeverity.Information),
                    new SPDiagnosticsCategory(LOG_WARNING,TraceSeverity.Monitorable,EventSeverity.Warning),
                    new SPDiagnosticsCategory(LOG_ERROR,TraceSeverity.High,EventSeverity.Error ),
                    new SPDiagnosticsCategory(LOG_ERRORCRITICAL,TraceSeverity.Unexpected,EventSeverity.ErrorCritical),
                     new SPDiagnosticsCategory(LOG_MEDIUM,TraceSeverity.Medium ,EventSeverity.Error )

            } ) };
            return areas;
        }

        /// <summary>
        /// This method is used for writing the log message for information.
        /// </summary>
        /// <param name="categoryName">categoryName:Name of category[ELMExceptionService:ELM_INFO] in this case</param>
        /// <param name="message">message:message that is to be logged in logger.</param>
        /// <remarks>
        /// The method is used to write the log message for information.Method only write the log information.
        /// </remarks>
        public static void LogInfo(string categoryName, string message)
        {
            SPDiagnosticsCategory category = LoggerService.GetLogger.Areas[PRODUCT_DIAGNOSTIC_NAME].Categories[categoryName];
            LoggerService.GetLogger.WriteTrace(0, category, category.TraceSeverity, message);
        }

        /// <summary>
        /// This method is used for writing the log message for error.
        /// </summary>
        /// <param name="categoryName">categoryName:Name of category[ELMExceptionService:ELM_ERROR] in this case</param>
        /// <param name="message">message:message that is to be logged in logger.</param>
        /// <remarks>
        /// The method is used to write the log message for error.Method only write the log information.
        /// </remarks>
        public static void LogError(string categoryName, string message)
        {
            SPDiagnosticsCategory category = LoggerService.GetLogger.Areas[PRODUCT_DIAGNOSTIC_NAME].Categories[categoryName];
            LoggerService.GetLogger.WriteTrace(0, category, category.TraceSeverity, message);
        }


        #endregion
    }
}
