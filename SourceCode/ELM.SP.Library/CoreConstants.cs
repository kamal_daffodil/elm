﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ELM.SP.Library
{
    public static class CoreConstants
    {
        public const string ImageHandlerURL = "/_layouts/ELM/ImageHandler/thumbnail.aspx?mode=";
        public const string CaptchaHandlerURL = "/_layouts/ELM/CaptchaHandler/Captcha.ashx?ct=";
        public static class Lists
        {
            public static class News
            {
                public const string URL = "News";
                public const string URLFromPages = "/Pages/Forms/AllItems.aspx";
                public const string PageContentTypeId = "0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900f2c35119514a476b9a802e9c80cb328e";
                public const string DefaultNewsArabicImageURL = "/Style%20Library/Images/NoImage-ar.jpg";
                public const string DefaultNewsEnglishImageURL ="/Style%20Library/Images/NoImage-eng.jpg";
                public static class Fields
                {
                    public const string NewsTitle = "Title";
                    public const string NewsModifiedDate = "Modified";
                    public const string NewsDescription = "ELMDescription";
                    public const string NewsDisplayOrder = "ELMDisplayOrder";
                    public const string NewsRedirectURL = "ELMRedirectURL";
                    public const string NewsImage = "ELMPublishingImage";
                    public const string NewsDate = "ELMNewsDate";
                    public const string NewsCategory = "ELMNewsCategory";
                    public const string NewsStartDate = "ELMStartDate";
                    public const string NewsEndDate = "ELMEndDate";
                    public const string NewsPageTitle = "LinkFilename";
                    public const string NewsPageContentType = "ContentTypeId";
                    
                }
            }

            public static class SubService 
            {
                public const string URL = "Lists/SubService";
                
                public static class Fields 
                {
                    public const string ServiceTitle = "Title";
                    public const string ServiceRedirectURL = "ELMRedirectURL";
                    public const string ServiceDisplayOrder = "ELMDisplayOrder";
                    public const string ServiceImage = "ELMPublishingImage";
                    public const string ServiceDescription = "ELMDescription";
                    public const string ServiceColorCode = "ELMTitleColorCode";
                }
            }

            public static class ContactFormRecord 
            {
                public const string URL = "Lists/ContactFormRecord";
                
                public static class Fields 
                {
                    public const string ContactTitle = "Title";
                    public const string ContactRequestType = "ELMRequestType";
                    public const string ContactService = "ELMService";
                    public const string ContactCompanyName = "ELMCompanyName";
                    public const string ContactBranchCity = "ELMBranchCity";
                    public const string ContactPersonTitle = "ELMTitle";
                    public const string ContactJobTitle = "ELMJobTitle";
                    public const string ContactPhone = "ELMPhone";
                    public const string ContactExtension = "ELMExtension";
                    public const string ContactMobile = "ELMMobile";
                    public const string ContactFax = "ELMFax";
                    public const string ContactEmail = "ELMEmail";
                    public const string ContactMessage = "ELMMessage";
                }
            }

            public static class CareerContactForm 
            {
                public const string URL = "Lists/CareerContactForm";
                
                public static class Fields 
                {
                    public const string CareerContactCountry = "ELMCountry";
                    public const string CareerContactCity = "ELMCity";
                    public const string CareerContactTitle = "ELMTitle";
                    public const string CareerContactMobile = "ELMMobile";
                    public const string CareerContactEmail = "ELMEmail";
                    public const string CareerContactMessage = "ELMMessage";
                    public const string CareerContactName = "Title";
                }
            }

            public static class Banner 
            {
                public const string URL = "bannerItems";
                public const string VoidURL = "javascript:void()";
                public const string NoItemString = "<h3>No item(s) found.</h3>";
                public static class Fields 
                {
                    public const string BannerTitle = "Title";
                    public const string BannerImage = "ELMPublishingImage";
                    public const string BannerDisplayOrder = "ELMDisplayOrder";
                    public const string BannerRedirectURL = "ELMRedirectURL";
                    public const string BannerStartDate = "ELMStartDate";
                    public const string BannerEndDate = "ELMEndDate";
                }
            }

            public static class Solution 
            {
                public const string URL = "Lists/ProjectSolutions";
                
                public static class Fields 
                {
                    public const string SolutionsTitle = "Title";
                    public const string SolutionsDescription = "ELMDescription";
                    public const string SolutionsDisplayOrder = "ELMDisplayOrder";
                    public const string SolutionsRedirectURL = "ELMRedirectURL";
                    public const string SolutionsIcon = "ELMPublishingIcon";
                    public const string SolutionsImage = "ELMPublishingImage";
                }
            }

            public static class TwitterFeeds 
            {
                public const string URL = "Lists/TwitterAccounts";
                public const string ELMFollowLink = "https://twitter.com/intent/follow?region=follow_link&screen_name=Elm";
                public const string TwitterKeyValue = "1";
                public const string ResourceURL = "https://api.twitter.com/1.1/statuses/user_timeline.json";
                public const string OAuthVersion = "1.0";
                public const string SignatureMethod = "HMAC-SHA1";
                
                public static class Fields 
                {
                    public const string OAuthToken = "ELMOAuthToken";
                    public const string OAuthTokenSecret = "ELMOAuthTokenSecret";
                    public const string OAuthConsumerKey = "ELMOAuthConsumerKey";
                    public const string OAuthConsumerSecret = "ELMOAuthConsumerSecret";
                    public const string ScreenName = "ELMTwitterScreenName";
                    public const string TwitterKey = "ELMTwitterKey";
                }
            }

            public static class Navigation
            {
                public const string URL = "Lists/NavigationLinks";
                public const string SideFooterContentType = "FooterSide";
                public const string CenterFooterContentType = "FooterCenter";
                public const string MenuNavigationContentType = "HeaderLinks";
                public const string SubMenuNavigationContentType = "SubHeading";
                
                public static class Fields 
                {
                    public const string NavigationTitle = "Title";
                    public const string NavigationRedirectURL = "ELMRedirectURL";
                    public const string NavigationDisplayOrder = "ELMDisplayOrder";
                    public const string NavigationSubHeadingParent = "ELMSubHeadingParent";
                    public const string NavigationSubHeadingDescription = "ELMSubHeadingDescription";
                    public const string NavigationImage = "ELMPublishingImage";
                    public const string NavigationContentType = "ContentType";
                }
            }

            public static class SocialAccounts 
            {
                public const string URL = "Lists/SocialMedia";

                public static class Fields 
                {
                    public const string AccountTitle = "Title";
                    public const string AccountRedirectURL = "ELMRedirectURL";
                    public const string AccountDisplayOrder = "ELMDisplayOrder";
                    public const string AccountImage = "ELMPublishingImage";
                }
            }

            public static class Subscription 
            {
                public const string URL = "Subscription";

                public static class Fields 
                {
                    public const string SubscriptionEMail = "ELMEmail";
                }
            }
        }
    }
}
