<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>


<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
			<div class="newsSection detailPL"><div class="newsDetailTop"><span class="date">          	
	          	<SharePointWebControls:datetimefield FieldName="ArticleStartDate" runat="server" id="datetimefield3"></SharePointWebControls:datetimefield>
          	</span>
          	<span class="newsHeading">          	
          	<PublishingWebControls:EditModePanel runat=server PageDisplayMode="Edit">
			    <SharePointWebControls:TextField FieldName="Title" CssClass="emedia-spwc-global" runat="server" />
			</PublishingWebControls:EditModePanel>

			 
				<SharePointWebControls:FieldValue FieldName="Title" CssClass="emedia-spwc-global" runat="server"/>
			
          	
          	</span></div><div class="newsDetailbottom">
          	  <div class="newsImg" >
          	 <span>
				<PublishingWebControls:RichImageField id="ContentQueryImage" FieldName="PublishingRollupImage" AllowHyperLinks="false" runat="server"/>
             </span>
          	 

          	  </div><p>
          	  &nbsp;<PublishingWebControls:RichHtmlField  FieldName="ELMDescription"  runat="server" ID="serviceDetailDescription" Text="scgfsdj"></PublishingWebControls:RichHtmlField>
          	  </p></div></div>
          <div class="clr"></div>

</asp:Content>

