<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=14.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:progid="SharePoint.WebPartPage.Document" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">

<style type="text/css">
.innerLeft{
	width:100%;
}

.innerRight{
	display:none;
}
</style>

<div class="bannerWrapper">
<WebPartPages:WebPartZone ID="TopHeader" runat="server"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
</div>

<div class="content-container">

<div class="topPane" >
<WebPartPages:WebPartZone ID="TopPane" runat="server"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
</div>

<div class="mainContent">

<div class="wrapper">
         <div class="eventsBlock">
         <WebPartPages:WebPartZone ID="EventsBlock" runat="server"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
         </div>
         
         <div class="bottomPane">

<div class="bottompaneleft">
<WebPartPages:WebPartZone ID="BottomPaneLeft" runat="server"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
</div>

<div class="bottompaneright">
<WebPartPages:WebPartZone ID="BottomPaneRight" runat="server"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
</div>


</div>

</div>
</div>


</div>
</asp:Content>
