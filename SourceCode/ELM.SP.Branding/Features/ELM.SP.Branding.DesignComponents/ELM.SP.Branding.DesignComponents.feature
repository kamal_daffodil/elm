﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="16f715c4-8af1-4165-8aaa-6d6ccec9ffd6" alwaysForceInstall="true" description="This will deploy the design components (css, js and images) in style library." featureId="16f715c4-8af1-4165-8aaa-6d6ccec9ffd6" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="ELM.SP.Branding.DesignComponents" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="12314ff5-58ec-46ee-b184-bd21b5aa92bf" />
    <projectItemReference itemId="8e66f763-64d8-4c09-a059-8187be165f0f" />
    <projectItemReference itemId="0bc7d927-c20a-441e-9eb7-7e1152735a34" />
  </projectItems>
</feature>